package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.MedicoController;

public class Grafico extends JPanel {

    private Pressao panel_pressao;
    private Batimentos panel_batimentos;

    /**
     * Create the frame.
     *
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public Grafico(ArrayList<String[]> historico) {
        setPreferredSize(new Dimension(634, 600));
        setLayout(null);

        JTextPane txtpnBatimentos = new JTextPane();
        txtpnBatimentos.setEditable(false);
        txtpnBatimentos.setFont(new Font("Roboto Light", Font.PLAIN, 16));
        txtpnBatimentos.setText("Batimentos");
        txtpnBatimentos.setBackground(getBackground());
        txtpnBatimentos.setBounds(10, 11, 92, 20);
        add(txtpnBatimentos);

        panel_batimentos = new Batimentos(historico);
        panel_batimentos.setBounds(10, 42, 600, 285);
        add(panel_batimentos);

        JTextPane txtpnPressoSangunea = new JTextPane();
        txtpnPressoSangunea.setEditable(false);
        txtpnPressoSangunea.setFont(new Font("Roboto Light", Font.PLAIN, 16));
        txtpnPressoSangunea.setText("Press\u00E3o Sangu\u00EDnea");
        txtpnPressoSangunea.setBackground(getBackground());
        txtpnPressoSangunea.setBounds(10, 338, 176, 25);
        add(txtpnPressoSangunea);

        JTextPane txtpnLabelVermelha = new JTextPane();
        txtpnLabelVermelha.setEditable(false);
        txtpnLabelVermelha.setBounds(238, 347, 20, 20);
        txtpnLabelVermelha.setBackground(new Color(255, 0, 0));
        add(txtpnLabelVermelha);

        JTextPane txtpnPressoSistlica = new JTextPane();
        txtpnPressoSistlica.setEditable(false);
        txtpnPressoSistlica.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnPressoSistlica.setText("Press\u00E3o Sist\u00F3lica");
        txtpnPressoSistlica.setBounds(273, 347, 104, 20);
        txtpnPressoSistlica.setBackground(getBackground());
        add(txtpnPressoSistlica);

        JTextPane txtpnLabelAzul = new JTextPane();
        txtpnLabelAzul.setEditable(false);
        txtpnLabelAzul.setBounds(413, 347, 20, 20);
        txtpnLabelAzul.setBackground(new Color(0, 102, 255));
        add(txtpnLabelAzul);

        JTextPane txtpnPressoDiastlica = new JTextPane();
        txtpnPressoDiastlica.setEditable(false);
        txtpnPressoDiastlica.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnPressoDiastlica.setText("Press\u00E3o Diast\u00F3lica");
        txtpnPressoDiastlica.setBounds(443, 347, 112, 20);
        txtpnPressoDiastlica.setBackground(getBackground());
        add(txtpnPressoDiastlica);

        panel_pressao = new Pressao(historico);
        panel_pressao.setBounds(10, 374, 600, 220);
        add(panel_pressao);

        panel_batimentos.scrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                panel_pressao.scrollPane.getHorizontalScrollBar().setValue(panel_batimentos.scrollPane.getHorizontalScrollBar().getValue());
                repaint();
            }
        });

        panel_pressao.scrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                panel_batimentos.scrollPane.getHorizontalScrollBar().setValue(panel_pressao.scrollPane.getHorizontalScrollBar().getValue());
                repaint();
            }
        });

    }
}
