package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import controller.MedicoController;

public class Batimentos extends JPanel {

    protected JScrollPane scrollPane;
    private ArrayList<String[]> dados;

    /**
     * Create the panel.
     */
    public Batimentos(ArrayList<String[]> historico) {
        setPreferredSize(new Dimension(600, 280));
        setLayout(null);

        this.dados = historico;

        scrollPane = new JScrollPane(new PanelPintura());
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(33, 11, 567, 270);
        add(scrollPane);

        scrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {

            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                repaint();
            }
        });

        JTextPane valorMinimo = new JTextPane();
        valorMinimo.setEditable(false);
        valorMinimo.setBounds(5, 200, 22, 19);
        valorMinimo.setText("0");
        valorMinimo.setBackground(getBackground());
        add(valorMinimo);

        JTextPane valorMaximo = new JTextPane();
        valorMaximo.setEditable(false);
        valorMaximo.setText("200");
        valorMaximo.setBounds(5, 0, 25, 20);
        valorMaximo.setBackground(getBackground());
        add(valorMaximo);

        JTextPane valorMedio = new JTextPane();
        valorMedio.setEditable(false);
        valorMedio.setText("100");
        valorMedio.setBounds(5, 100, 22, 20);
        valorMedio.setBackground(getBackground());
        add(valorMedio);
    }

    private class PanelPintura extends JPanel {

        private int xAnterior;
        private int yAnterior;
        private ArrayList<int[]> posicoesAntigas = new ArrayList<>();
        private ArrayList<String[]> textoAntigo = new ArrayList<>();
        private int i;

        public PanelPintura() {
            int largura = (dados == null) ? 600 : ((dados.size() - 2) * 200) + 150;
            setPreferredSize(new Dimension(largura, 200));
            setVisible(true);
        }

        public void paint(Graphics g) {

            Graphics2D g2d = (Graphics2D) g;
            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g2d.setRenderingHints(rh);

            g2d.setColor(Color.black);
            g2d.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

            Iterator<int[]> iterador = posicoesAntigas.iterator();

            int texto = 0;
            while (iterador.hasNext()) {
                int[] antigo = iterador.next();

                g2d.drawLine(antigo[0], antigo[1], antigo[2], antigo[3]);

                if (dados.size() > texto) {
                    if (dados.get(texto)[0] != null) {

                        g2d.setColor(Color.black);
                        if (antigo[1] < antigo[3]) {
                            if (texto == 0) {
                                g2d.drawString(textoAntigo.get(texto)[2], antigo[2] + 3, antigo[3] - 10);
                            } else {
                                g2d.drawString(textoAntigo.get(texto)[2], antigo[2], antigo[3] - 10);
                            }

                        } else {
                            if (texto == 0) {
                                g2d.drawString(textoAntigo.get(texto)[2], antigo[2] + 3, antigo[3] + 20);
                            } else {
                                g2d.drawString(textoAntigo.get(texto)[2], antigo[2] - 7, antigo[3] + 20);
                            }
                        }

                        g2d.fillOval(antigo[2] - 5, antigo[3] - 5, 10, 10);

                        g2d.setColor(Color.black);
                        g2d.drawString(textoAntigo.get(texto)[0], Integer.parseInt(textoAntigo.get(texto)[1]), 220);
                    }
                    texto++;
                }

            }

            if (dados != null) {
                if (posicoesAntigas.size() <= 1) {
                    xAnterior = -15;
                    yAnterior = Integer.parseInt(dados.get(1)[1]);
                }

                for (int j = 0; j < dados.size(); j++) {
                    String[] atual = dados.get(j);

                    if (atual[1] != null) {
                        int yNovo = Integer.parseInt(atual[1]);

                        if (i < this.getWidth() + 50) {
                            g2d.drawLine(xAnterior, yAnterior, i, 200 - yNovo);
                            int[] posicaoAtual = {xAnterior, yAnterior, i, 200 - yNovo};
                            posicoesAntigas.add(posicaoAtual);
                            textoAntigo.add(new String[]{atual[0], Integer.toString(i), Integer.toString(yNovo)});

                            xAnterior = i;
                            yAnterior = 200 - yNovo;
                            i += 200;
                        } else {
                            return;
                        }
                    }

                }
            }

        }
    }
}
