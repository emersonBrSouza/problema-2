package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.MedicoController;

public class ResultadoView extends JFrame {

    private Timer timer;
    private JPanel contentPane;
    private String idPaciente;
    private Timer timerGrafico;
    private JTabbedPane tabbedPane;

    private JScrollPane scrollPane;

    /**
     * Create the frame.
     *
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public ResultadoView(String nomePaciente, String idPaciente) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        setTitle("Hist\u00F3rico de " + nomePaciente + "-" + idPaciente);
        setResizable(false);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }

        this.idPaciente = idPaciente;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 750, 550);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(10, 11, 724, 499);
        contentPane.add(tabbedPane);

        JComponent historicoTabela = new Historico(idPaciente);

        scrollPane = new JScrollPane(new Grafico(MedicoController.getInstance().getHistorico()));
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JComponent historicoGrafico = scrollPane;
		//JComponent historicoGrafico = new Grafico(idPaciente);

        tabbedPane.addTab("Histórico", historicoTabela);
        tabbedPane.addTab("Gráfico", historicoGrafico);

        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                timer.cancel();
            }

        });
        setVisible(true);
    }

    public class AtualizarGrafico extends TimerTask {

        @Override
        public void run() {
            MedicoController.getInstance().recuperarHistorico(idPaciente);
        }
    }

    private class Historico extends JPanel implements Observer {

        private JTable table;
        private String idPaciente;

        /**
         * Create the frame.
         */
        public Historico(String idPaciente) {
            setBackground(Color.WHITE);
            setLayout(null);

            this.idPaciente = idPaciente;

            setTitle("Hist\u00F3rico:" + this.idPaciente);
            JTextPane txtpnHistricoDosPerodos = new JTextPane();
            txtpnHistricoDosPerodos.setEditable(false);
            txtpnHistricoDosPerodos.setFont(new Font("Roboto Light", Font.PLAIN, 16));
            txtpnHistricoDosPerodos.setText("Hist\u00F3rico dos per\u00EDodos de risco");
            txtpnHistricoDosPerodos.setBounds(10, 11, 500, 20);
            add(txtpnHistricoDosPerodos);

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setBounds(10, 50, 695, 410);
            add(scrollPane);

            MedicoController.getInstance().addObserver(this);

            table = new JTable();
            table.setModel(new DefaultTableModel(
                    new Object[][]{},
                    new String[]{
                        "Hor\u00E1rio", "Batimentos", "Press\u00E3o Sist\u00F3lica", "Press\u00E3o Diast\u00F3lica", "Em Repouso"
                    }) {
                        /**
                         *
                         */
                        private static final long serialVersionUID = 1L;
                        boolean[] columnEditables = new boolean[]{
                            false, false, false, false, false
                        };

                        public boolean isCellEditable(int row, int column) {
                            return columnEditables[column];
                        }
                    });
            scrollPane.setViewportView(table);

            TimerTask t = new RequisitarAtualizacoes();
            timer = new Timer();
            timer.schedule(t, 0, 30000);

            setVisible(true);
        }

        public class RequisitarAtualizacoes extends TimerTask {

            @Override
            public void run() {
                MedicoController.getInstance().recuperarHistorico(idPaciente);
            }
        }

        @Override
        public void update(Observable o, Object arg) {
            if (o != null && arg instanceof ArrayList) {
                if (arg != null && arg.equals(MedicoController.getInstance().getHistorico())) {
                    try {
                        scrollPane.setViewportView(new Grafico(MedicoController.getInstance().getHistorico()));
                        atualizarTabela();
                        repaint();
                    } catch (Exception e) {

                    }
                }
            }
        }

        //Atualizar Listas
        private void atualizarTabela() {
            ArrayList<String[]> historico = MedicoController.getInstance().getHistorico();
            Iterator<String[]> iterador = historico.iterator();

            removerTodos(table);

            while (iterador.hasNext()) {
                String[] valor = iterador.next();
                adicionarItem(table, valor);
            }
        }

        private void adicionarItem(JTable tabela, String[] valor) {
            DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
            String repouso = "";

            if (valor[4] == null) {
                return;
            }
            if (valor[4].equals("true")) {
                repouso = "Sim";
            } else {
                repouso = "N\u00E3o";
            }

            dtm.insertRow(0, new Object[]{valor[0], valor[1], valor[2], valor[3], repouso});

        }

        private void removerTodos(JTable tabela) {
            DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
            for (int index = 0; index < tabela.getModel().getRowCount(); index++) {
                dtm.removeRow(0);
            }
        }
    }

}
