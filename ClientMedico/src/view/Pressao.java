package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextPane;

public class Pressao extends JPanel {

    protected JScrollPane scrollPane;
    private ArrayList<String[]> dados;

    /**
     * Create the panel.
     */
    public Pressao(ArrayList<String[]> historico) {
        setBounds(0, 0, 600, 230);
        setLayout(null);

        this.dados = historico;

        scrollPane = new JScrollPane(new PanelPintura());
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(32, 11, 568, 202);
        add(scrollPane);

        JTextPane txtpnValorminimo = new JTextPane();
        txtpnValorminimo.setText("0");
        txtpnValorminimo.setEditable(false);
        txtpnValorminimo.setBounds(5, 166, 12, 20);
        txtpnValorminimo.setBackground(getBackground());
        add(txtpnValorminimo);

        JTextPane txtpnValormaximo = new JTextPane();
        txtpnValormaximo.setText("40");
        txtpnValormaximo.setEditable(false);
        txtpnValormaximo.setBounds(5, 0, 17, 20);
        txtpnValormaximo.setBackground(getBackground());
        add(txtpnValormaximo);

        JTextPane txtpnValorMedio = new JTextPane();
        txtpnValorMedio.setText("20");
        txtpnValorMedio.setBounds(5, 80, 17, 20);
        txtpnValorMedio.setBackground(getBackground());
        add(txtpnValorMedio);
    }

    private class PanelPintura extends JPanel {

        private int xAnteriorSistolica;
        private int yAnteriorSistolica;
        private int xAnteriorDiastolica;
        private int yAnteriorDiastolica;

        private ArrayList<int[]> posicoesAntigasSistolica = new ArrayList<>();
        private ArrayList<int[]> posicoesAntigasDiastolica = new ArrayList<>();

        private ArrayList<String[]> textoAntigoSistolica = new ArrayList<>();
        private ArrayList<String[]> textoAntigoDiastolica = new ArrayList<>();

        private int i;

        public PanelPintura() {
            int largura = (dados == null) ? 600 : ((dados.size() - 2) * 200) + 150;
            setPreferredSize(new Dimension(largura, 180));
            setVisible(true);
        }

        public void paint(Graphics g) {

            Graphics2D g2d = (Graphics2D) g;
            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setRenderingHints(rh);

            g2d.setColor(Color.black);
            g2d.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

            Iterator<int[]> iteradorSistolica = posicoesAntigasSistolica.iterator();
            Iterator<int[]> iteradorDiastolica = posicoesAntigasDiastolica.iterator();

            int texto = 0;
            while (iteradorSistolica.hasNext()) {
                int[] antigaSistolica = iteradorSistolica.next();
                int[] antigaDiastolica = iteradorDiastolica.next();

                g2d.setColor(new Color(255, 0, 0));
                g2d.drawLine(antigaSistolica[0], antigaSistolica[1], antigaSistolica[2], antigaSistolica[3]);

                g2d.setColor(new Color(0, 102, 255));
                g2d.drawLine(antigaDiastolica[0], antigaDiastolica[1], antigaDiastolica[2], antigaDiastolica[3]);

                if (dados.size() > texto) {
                    if (dados.get(texto)[0] != null) {

                        g2d.setColor(new Color(255, 0, 0));

                        if (antigaSistolica[1] < antigaSistolica[3]) {
                            if (texto == 0) {
                                g2d.drawString(textoAntigoSistolica.get(texto)[2], antigaSistolica[2] + 3, antigaSistolica[3] - 10);
                            } else {
                                g2d.drawString(textoAntigoSistolica.get(texto)[2], antigaSistolica[2], antigaSistolica[3] - 10);
                            }

                        } else {
                            if (texto == 0) {
                                g2d.drawString(textoAntigoSistolica.get(texto)[2], antigaSistolica[2] + 3, antigaSistolica[3] - 10);
                            } else {
                                g2d.drawString(textoAntigoSistolica.get(texto)[2], antigaSistolica[2] - 7, antigaSistolica[3] - 10);
                            }
                        }

                        g2d.fillOval(antigaSistolica[2] - 5, antigaSistolica[3] - 5, 10, 10);

                        g2d.setColor(new Color(0, 102, 255));
                        if (antigaDiastolica[1] < antigaDiastolica[3]) {
                            if (texto == 0) {
                                g2d.drawString(textoAntigoDiastolica.get(texto)[2], antigaDiastolica[2] + 3, antigaDiastolica[3] + 20);
                            } else {
                                g2d.drawString(textoAntigoDiastolica.get(texto)[2], antigaDiastolica[2], antigaDiastolica[3] + 20);
                            }

                        } else {
                            if (texto == 0) {
                                g2d.drawString(textoAntigoDiastolica.get(texto)[2], antigaDiastolica[2] + 3, antigaDiastolica[3] + 20);
                            } else {
                                g2d.drawString(textoAntigoDiastolica.get(texto)[2], antigaDiastolica[2] - 7, antigaDiastolica[3] + 20);
                            }
                        }

                        g2d.fillOval(antigaDiastolica[2] - 5, antigaDiastolica[3] - 5, 10, 10);

                        g2d.setColor(Color.black);
                        g2d.drawString(textoAntigoSistolica.get(texto)[0], Integer.parseInt(textoAntigoSistolica.get(texto)[1]), 180);
                    }
                    texto++;
                }

            }

            if (dados != null) {
                if (posicoesAntigasSistolica.size() <= 1) {
                    xAnteriorSistolica = -15;
                    yAnteriorSistolica = Integer.parseInt(dados.get(1)[2]);
                }

                if (posicoesAntigasDiastolica.size() <= 1) {
                    xAnteriorDiastolica = -15;
                    yAnteriorDiastolica = Integer.parseInt(dados.get(1)[3]);
                }

                for (int j = 0; j < dados.size(); j++) {
                    String[] atual = dados.get(j);

                    if (atual[1] != null) {
                        int yNovoSistolica = Integer.parseInt(atual[2]);
                        int yNovoDiastolica = Integer.parseInt(atual[3]);
                        if (i < this.getWidth() + 50) {

                            g2d.drawLine(xAnteriorSistolica, yAnteriorSistolica, i, 160 - (yNovoSistolica * 4));
                            int[] posicaoAtualSistolica = {xAnteriorSistolica, yAnteriorSistolica, i, 160 - (yNovoSistolica * 4)};
                            posicoesAntigasSistolica.add(posicaoAtualSistolica);
                            textoAntigoSistolica.add(new String[]{atual[0], Integer.toString(i), Integer.toString(yNovoSistolica)});

                            xAnteriorSistolica = i;
                            yAnteriorSistolica = 160 - (yNovoSistolica * 4);

                            g2d.drawLine(xAnteriorSistolica, yAnteriorSistolica, i, 160 - (yNovoDiastolica * 4));
                            int[] posicaoAtualDiastolica = {xAnteriorDiastolica, yAnteriorDiastolica, i, 160 - (yNovoDiastolica * 4)};
                            posicoesAntigasDiastolica.add(posicaoAtualDiastolica);
                            textoAntigoDiastolica.add(new String[]{atual[0], Integer.toString(i), Integer.toString(yNovoDiastolica)});

                            xAnteriorDiastolica = i;
                            yAnteriorDiastolica = 160 - (yNovoDiastolica * 4);

                            i += 200;
                        } else {
                            return;
                        }
                    }

                }
            }
        }
    }
}
