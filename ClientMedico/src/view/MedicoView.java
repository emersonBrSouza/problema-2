package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.RowSorter;
import javax.swing.ScrollPaneConstants;
import javax.swing.SortOrder;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controller.MedicoController;
import model.Paciente;
import model.Sensor;

public class MedicoView extends JFrame implements Observer {

    private String nomeMedico;
    private String crm;
    private static MedicoController controller = MedicoController.getInstance();
    private JTextField textField;
    private JTable tabelaPacientes;
    private ListarTodosPacientes listarTodosPacientes;
    private VerificarConexao verificarConexao;

    /**
     * Create the frame.
     *
     * @param crm
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public static void main() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        new MedicoView("ata", "ata", controller);
    }

    public MedicoView(String nome, final String crm, final MedicoController controller) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        getContentPane().setBackground(Color.WHITE);
        this.nomeMedico = nome;
        this.crm = crm;
        this.controller = controller;

        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 720, 480);

        setVisible(true);
        setResizable(false);
        getContentPane().setLayout(null);

        new PacientesTotais().inicializar();

        controller.addObserver(this);

        verificarConexao = new VerificarConexao();
        Timer timerVerificacao = new Timer();
        timerVerificacao.schedule(verificarConexao, 0, 5000);

        listarTodosPacientes = new ListarTodosPacientes();
        Timer timerTodosPacientes = new Timer();
        timerTodosPacientes.schedule(listarTodosPacientes, 0, 5000);

    }

    private class VerificarConexao extends TimerTask {

        public void run() {
            controller.verificarConexao();
        }
    }

    //Povoamento das tabelas
    private class ListarTodosPacientes extends TimerTask {

        public void run() {
            controller.listarTodosPacientes();
        }
    }

    //Atualizar Listas
    private void atualizarTodosPacientes() {
        ArrayList<Paciente> pacientes = controller.getTodosPacientes();
        Iterator<Paciente> iterador = pacientes.iterator();

        while (iterador.hasNext()) {
            Paciente p = (Paciente) iterador.next();
            adicionarItem(tabelaPacientes, p);
        }
    }

    private void monitorarEspecifico(JTable tabela) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        int index = tabela.getSelectedRow();
        String idPaciente = (String) tabela.getModel().getValueAt(index, 0);
        new MonitoramentoView(this, idPaciente);
    }

	//Manipulação das tabelas
    private void adicionarItem(JTable tabela, Paciente item) {
        DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
        if (item == null) {
            return;
        }

        Sensor s = item.getSensor();
        String repouso = s.estaEmRepouso() ? "Sim" : "Não";

        int index = buscarID(tabela, item);
        if (index == -1) { //Se não existir, adiciona
            dtm.addRow(new String[]{s.getId(),
                item.getNome(),
                Integer.toString(s.getBatimentos()),
                Integer.toString(s.getPressaoSistolica()),
                Integer.toString(s.getPressaoDiastolica()),
                repouso});
        } else { //Se existir atualiza
            dtm.setValueAt(item.getNome(), index, 1);
            dtm.setValueAt(s.getBatimentos(), index, 2);
            dtm.setValueAt(s.getPressaoSistolica(), index, 3);
            dtm.setValueAt(s.getPressaoDiastolica(), index, 4);
            dtm.setValueAt(repouso, index, 5);
        }
    }

    private int buscarID(JTable tabela, Paciente item) {
        DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
        int n = dtm.getRowCount();

        for (int i = 0; i < n; i++) {
            if (dtm.getValueAt(i, 0).equals(item.getSensor().getId())) {
                return i;
            }
        }
        return -1;
    }

    private void removerTodos(JTable tabela) {
        DefaultTableModel dtm = (DefaultTableModel) tabela.getModel();
        for (int index = 0; index < tabela.getModel().getRowCount(); index++) {
            dtm.removeRow(0);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Boolean) {
            if (!MedicoController.getInstance().isConectadoAoServidor()) {
                System.err.println("Você foi desconectado do servidor. O programa está tentando reconectar.");
                return;
            }
        }

        if (o instanceof MedicoController && arg instanceof ArrayList) {

            if (((MedicoController) o).getTodosPacientes() != null) {
                if (((MedicoController) o).getTodosPacientes().size() == 0) {
                    removerTodos(tabelaPacientes);
                } else {
                    removerTodos(tabelaPacientes);
                    atualizarTodosPacientes();
                }
            }
        }
    }

    public MedicoController getController() {
        return this.controller;
    }

    private class PacientesTotais {

        public void inicializar() {

            JTextPane txtpnPacientesConectados = new JTextPane();
            txtpnPacientesConectados.setText("Pacientes Propensos");
            txtpnPacientesConectados.setFont(new Font("Roboto Light", Font.PLAIN, 14));
            txtpnPacientesConectados.setBackground(getContentPane().getBackground());
            txtpnPacientesConectados.setEditable(false);
            txtpnPacientesConectados.setBounds(10, 11, 200, 20);
            getContentPane().add(txtpnPacientesConectados);

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setBounds(10, 47, 677, 389);
            getContentPane().add(scrollPane);

            tabelaPacientes = new JTable();
            tabelaPacientes.setModel(new DefaultTableModel(
                    new String[][]{},
                    new String[]{
                        "ID do Dispositivo", "Nome do Paciente", "Batimentos", "Pressão Sistólica", "Pressão Diastólica", "Em repouso"
                    }
            ) {
                /**
                 *
                 */
                private static final long serialVersionUID = 1L;
                boolean[] columnEditables = new boolean[]{
                    false, false, false, false, false, false
                };

                public boolean isCellEditable(int row, int column) {
                    return columnEditables[column];
                }
            });

            tabelaPacientes.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2 && !e.isConsumed()) {
                        e.consume();
                        try {
                            monitorarEspecifico(tabelaPacientes);
                        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {

                        }
                    }
                }
            });
            scrollPane.setViewportView(tabelaPacientes);

            tabelaPacientes.getColumnModel().getColumn(0).setPreferredWidth(100);
            tabelaPacientes.getColumnModel().getColumn(1).setPreferredWidth(100);
            tabelaPacientes.getColumnModel().getColumn(2).setPreferredWidth(60);
            tabelaPacientes.getColumnModel().getColumn(3).setPreferredWidth(85);
            tabelaPacientes.getColumnModel().getColumn(4).setPreferredWidth(85);
            tabelaPacientes.getColumnModel().getColumn(5).setPreferredWidth(60);

            TableRowSorter<TableModel> comparador = new TableRowSorter<TableModel>(tabelaPacientes.getModel());

            List<RowSorter.SortKey> chaves = new ArrayList<>();
            chaves.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
            comparador.setSortKeys(chaves);

            tabelaPacientes.setRowSorter(comparador);
        }
    }
}
