package controller;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import model.Paciente;
import util.Acao;

public class MedicoController extends Observable {

    private static MedicoController controller;
    private static String id;
    private String ipNuvem;
    private int portaNuvem;
    private ArrayList<Paciente> todosPacientes;
    private ArrayList<String[]> historico = new ArrayList<String[]>();
    private Map<String, Paciente> pacientesEspecificos = new HashMap<String, Paciente>();
    private DatagramSocket socketUDPMedico;
    private boolean conectadoAoServidor = false;

    //Singleton
    private MedicoController() {
    }

    public static MedicoController getInstance() {
        if (controller == null) {
            controller = new MedicoController();
        }
        return controller;
    }

    /**
     * Realiza o login do médico
     *
     * @param username - O nome do usu�rio
     * @param password - A senha do usu�rio
     * @param enderecoIP - O endere�o IP do servidor
     * @param porta - A porta do servidor
     * @return true - Se o login for bem-sucedido
     * @return false - Se o login for mal-sucedido
     */
    public boolean login(String username, String password, String enderecoIP, int porta) {
        this.ipNuvem = enderecoIP;
        this.portaNuvem = porta;

        Acao acao = new Acao("--login", username + ":" + password + ":" + id);
        boolean resposta = false;

        try {
            Socket clienteSocket = new Socket(enderecoIP, porta);
            ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream());
            BufferedReader entrada = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));

            saida.writeObject(acao);
            saida.flush();

            String mensagem = entrada.readLine();
            //Verifica se o login foi bem-sucedido
            if (mensagem.equals("--login-accepted")) {
                resposta = true;

            } else if (mensagem.equals("--login-refused")) {
                resposta = false;
            }

            clienteSocket.close();

        } catch (UnknownHostException e) {
            System.err.println("Servidor n�o encontrado");
            return false;
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunica��o");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Erro no envio dos dados");
            return false;
        } catch (Exception e) {
            System.err.println("Erro desconhecido");
            return false;
        }

        return resposta;
    }

    /**
     * Realiza o login do médico
     *
     * @param idMedico - O id do usuário
     * @return true - Se o login for bem-sucedido
     * @return false - Se o login for mal-sucedido
     */
    public boolean logout(String idMedico) {

        Acao acao = new Acao("--logout", "Médico" + ":" + idMedico);
        boolean resposta = false;

        try {
            Socket clienteSocket = new Socket(ipNuvem, portaNuvem);
            ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream()); //Obtém o canal de saída
            BufferedReader entrada = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));  //Obtém o canal de entrada

            //Escreve a ação e envia
            saida.writeObject(acao);
            saida.flush();

            String mensagem = entrada.readLine();

            if (mensagem.contains("--logout-accepted")) {
                resposta = true;

            }

            clienteSocket.close();

        } catch (UnknownHostException e) {
            System.err.println("Servidor n�o encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunica��o");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Erro no envio dos dados");
        }

        return true;
    }

    /**
     * Verifica periodicamente o estado da conexão
     */
    public void verificarConexao() {
        new Thread() {
            public void run() {
                try {
                    Socket clienteSocket = new Socket(ipNuvem, portaNuvem);

                    OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
                    clienteSocket.setSoTimeout(8000); // Define o tempo limite para a resposta.
                    saida.write(serializarMensagens(new Acao("--ping-doctor", id))); // "Pinga" o servidor
                    saida.flush();

                    InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
                    Object resposta = ((ObjectInputStream) entrada).readObject(); // Lê o objeto da entrada

                    if (resposta instanceof String) { //Verifica a resposta do servidor 
                        if (resposta.equals("ok")) {
                            setConectadoAoServidor(true);
                            setChanged();
                            notifyObservers(isConectadoAoServidor()); //Notifica aos observadores
                        }
                    }
                    saida.close();
                    clienteSocket.close();

                } catch (SocketTimeoutException e) {
                    setConectadoAoServidor(false);
                    setChanged();
                    notifyObservers(isConectadoAoServidor());
                } catch (SocketException e) {
                    setConectadoAoServidor(false);
                    setChanged();
                    notifyObservers(isConectadoAoServidor());
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Solicita ao servidor a seleção de um paciente
     *
     * @param idPaciente - O ID do paciente selecionado
     */
    public void selecionarPaciente(String idPaciente) {
        enviarUDP(new Acao("--select", id + ":" + idPaciente)); // Cria a ação a ser executada e envia
    }

    /**
     * Solicita ao servidor a lista de todos os pacientes propensos
     */
    public void listarTodosPacientes() {
        enviarUDP(new Acao("--list", "-all")); // Cria a ação a ser executada e envia
    }

    /**
     * Solicita ao servidor os dados de pacientes específicos
     *
     * @param idPaciente - O ID do paciente monitorado
     */
    public void monitorarEspecifico(final String idPaciente, final String ip, final int porta) {
        new Thread() {
            public void run() {
                enviarUDP(new Acao("--watch", id + ":" + idPaciente), ip, porta); // Cria a ação a ser executada e envia

            }
        }.start();
    }

    /**
     * Requisita ao servidor nuvem o servidor de borda em que se encontra
     * conectado o sensor
     *
     * @param idPaciente - O id do sensor a ser buscado
     * @return credenciais - O array contendo o ip e a porta do servidor de
     * borda
	 *
     */
    public String[] encontrarSensor(String idPaciente) {
        try {
            Socket clienteSocket = new Socket(ipNuvem, portaNuvem);

            OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
            clienteSocket.setSoTimeout(8000); // Define o tempo limite para a resposta.
            saida.write(serializarMensagens(new Acao("--search-sensor", idPaciente))); // "Pinga" o servidor
            saida.flush();

            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
            Object resposta = ((ObjectInputStream) entrada).readObject(); // Lê o objeto da entrada

            while (true) {
                if (resposta instanceof Object[]) { //Verifica a resposta do servidor
                    saida.close();
                    clienteSocket.close();

                    Object[] credenciais = (Object[]) resposta;
                    String[] retorno = {credenciais[0].toString(), credenciais[1].toString()};

                    return retorno;
                } else if (resposta instanceof String) {
                    String resp = (String) resposta;
                    if (resp.equals("--not-found")) {
                        saida.close();
                        clienteSocket.close();
                        return null;
                    }
                }
            }

        } catch (SocketTimeoutException e) {
            setConectadoAoServidor(false);
            setChanged();
            notifyObservers(isConectadoAoServidor());
        } catch (SocketException e) {
            setConectadoAoServidor(false);
            setChanged();
            notifyObservers(isConectadoAoServidor());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Envia uma requisição ao servidor
     */
    public void enviarUDP(Object mensagem) {
        byte[] dados = serializarMensagens(mensagem);

        try {
            socketUDPMedico = new DatagramSocket();
            DatagramPacket pacote = new DatagramPacket(dados, dados.length, InetAddress.getByName(ipNuvem), portaNuvem); //Configura o pacote
            socketUDPMedico.send(pacote); //Envia a mensagem
            ouvirUDP();
        } catch (UnknownHostException e) {
            System.err.println("Servidor não encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            System.err.println("Erro no envio dos dados");
        }
    }

    /**
     * Envia uma requisição ao servidor
     */
    public void enviarUDP(Object mensagem, String ip, int porta) {
        byte[] dados = serializarMensagens(mensagem);

        try {
            socketUDPMedico = new DatagramSocket();
            DatagramPacket pacote = new DatagramPacket(dados, dados.length, InetAddress.getByName(ip), porta); //Configura o pacote
            socketUDPMedico.send(pacote); //Envia a mensagem
            ouvirUDP();
        } catch (UnknownHostException e) {
            System.err.println("Servidor não encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            System.err.println("Erro no envio dos dados");
        }
    }

    /**
     * Recebe as mensagens que chegam via UDP
     */
    private void ouvirUDP() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        byte[] dadosRecebidos = new byte[1024];
                        DatagramPacket pacote = new DatagramPacket(dadosRecebidos, dadosRecebidos.length); //Configura o pacote
                        socketUDPMedico.receive(pacote); //Aguarda o recebimento do dado
                        new Thread(new Interpretador(pacote)).start(); // Inicia uma thread para o processamento do pacote recebido
                    } catch (EOFException e) {
                        socketUDPMedico.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Transforma uma mensagem em um array de bytes.
     *
     * @param mensagem - A mensagem a ser transformada.
     * @return byte[] - Um array de bytes convertidos
     */
    public byte[] serializarMensagens(Object mensagem) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            ObjectOutput out = new ObjectOutputStream(b);
            out.writeObject(mensagem);
            out.flush();
            return b.toByteArray();
        } catch (IOException e) {
            System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }

    public ArrayList<Paciente> getTodosPacientes() {
        return todosPacientes;
    }

    public void setTodosPacientes(ArrayList<Paciente> todosPacientes) {
        this.todosPacientes = todosPacientes;
        System.out.println("Recebendo " + todosPacientes.size() + " paciente(s) propenso(s)");
        setChanged();
        MedicoController.getInstance().notifyObservers(getTodosPacientes());
    }

    public Map<String, Paciente> getPacientesEspecificos() {
        return pacientesEspecificos;
    }

    protected void setPacientesEspecificos(Map<String, Paciente> pacientesEspecificos) {
        this.pacientesEspecificos = pacientesEspecificos;
    }

    public void setCRM(String crm) {
        MedicoController.id = crm;
    }

    public void informaMudanca(String id, Paciente paciente) {
        getPacientesEspecificos().put(id, paciente);
        setChanged();
        MedicoController.getInstance().notifyObservers(getPacientesEspecificos());
    }

    /**
     * Solicita ao servidor o histórico de um paciente específico
     *
     * @param idPaciente - O id do paciente que terá o histórico recuperado
     */
    public void recuperarHistorico(String idPaciente) {

        Acao acao = new Acao("--history", idPaciente); //Instancia a ação a ser requisitada

        try {
            final Socket clienteSocket = new Socket(ipNuvem, portaNuvem);
            ObjectOutputStream saida = new ObjectOutputStream(clienteSocket.getOutputStream()); //Obtém o canal de saída

            //Escreve a mensagem e envia
            saida.writeObject(acao);
            saida.flush();

            new Thread() {

                public void run() {
                    try {
                        InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); //Obtém o canal de entrada

                        Object mensagem = ((ObjectInput) entrada).readObject(); //Lê a entrada
                        ArrayList<String[]> historico = new ArrayList<String[]>();

                        if (mensagem instanceof ArrayList) { //Verifica a resposta
                            historico = (ArrayList<String[]>) mensagem;
                            setHistorico(historico); //Salva o histórico
                        }

                    } catch (IOException | ClassNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }.start();

        } catch (UnknownHostException e) {
            System.err.println("Servidor não encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Erro no envio dos dados");
        }

    }

    public ArrayList<String[]> getHistorico() {
        return this.historico;
    }

    /**
     * Define o histórico e atualiza os observadores
     *
     * @param historico - O histórico a ser armazenado
     */
    private void setHistorico(ArrayList<String[]> historico) {
        this.historico = historico;
        setChanged();
        notifyObservers(this.historico);
    }

    /**
     * Retorna o estado da conexão com o servidor
     *
     * @return true - Se estiver conectado ao servidor
     * @return false - Se não estiver conectado ao servidor
     */
    public Boolean isConectadoAoServidor() {
        return conectadoAoServidor;
    }

    public void setConectadoAoServidor(boolean conectadoAoServidor) {
        this.conectadoAoServidor = conectadoAoServidor;
    }
}
