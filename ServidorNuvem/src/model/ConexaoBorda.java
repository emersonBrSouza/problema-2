package model;

public class ConexaoBorda {

    private String ip;
    private int x;
    private int y;
    private int porta;
    private long ultimaAtualizacao;
    private boolean estavel;

    public ConexaoBorda(String ip, int x, int y, int porta, long ultimaAtualizacao) {
        this.ip = ip;
        this.x = x;
        this.y = y;
        this.porta = porta;
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    public long getUltimaAtualizacao() {
        return ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(long ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

    public boolean isEstavel() {
        return estavel;
    }

    public void setEstavel(boolean estavel) {
        this.estavel = estavel;
    }
}
