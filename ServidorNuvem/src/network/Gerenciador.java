package network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import controller.ServidorController;
import model.Paciente;
import model.Sensor;
import util.Acao;

public class Gerenciador implements Runnable {

    private DatagramPacket pacote;
    private InetAddress ip;
    private int porta;
    private Object mensagem;
    private Socket socketServidor;

    /**
     * Construtor para requisições via UDP
     *
     * @param pacote - o pacote recebido
     */
    public Gerenciador(DatagramPacket pacote) {
        this.pacote = pacote;
        this.ip = pacote.getAddress();
        this.porta = pacote.getPort();
        mensagem = desserializarMensagem(pacote.getData());

    }

    /**
     * Construtor para requisições via TCP
     *
     * @param socket - O socket da conexão
     */
    public Gerenciador(Socket socket) {
        this.socketServidor = socket;
        try {
            InputStream tcpInput = new ObjectInputStream(socketServidor.getInputStream());

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream obj = new ObjectOutputStream(bytes);

            try {
                obj.writeObject(((ObjectInputStream) tcpInput).readObject());
                bytes.toByteArray();
                mensagem = desserializarMensagem(bytes.toByteArray());
            } catch (ClassNotFoundException e) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Transforma um array de bytes em um Object.
     *
     * @param data - O array de bytes recebido.
     * @return Object - O objeto desserializado.
     */
    private Object desserializarMensagem(byte[] data) {
        ByteArrayInputStream mensagem = new ByteArrayInputStream(data);

        try {
            ObjectInput leitor = new ObjectInputStream(mensagem);
            return (Object) leitor.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run() {

        /*Define as decisões a serem tomadas a partir das requisições recebidas*/
        if (mensagem instanceof Acao) {
            Acao acaoRequisitada = (Acao) mensagem; // A ação recebida

            if (acaoRequisitada.getAcao().equals("--connect-sensor-cloud")) { // Conexão entre sensor e nuvem

                if (acaoRequisitada.getObjeto() instanceof Object[]) { // Verifica o servidor de borda mais próximo do sensor
                    Object[] recebido = (Object[]) acaoRequisitada.getObjeto();

                    int[] dados = (int[]) recebido[0]; // dados[0] =  X ; dados[1] = Y
                    String idSensor = (String) recebido[1];

                    System.out.println("Buscando o servidor de borda mais próximo do sensor em X: " + dados[0] + " e Y: " + dados[1]);

                    String[] credenciais = ServidorController.getInstance().calcularRota(dados[0], dados[1]); //[0] IP ; [1] Porta

                    if (credenciais.length == 0 || credenciais[0] == null) { //Se nenhum servidor de borda estiver disponível a operação falha
                        ServidorController.getInstance().responderTCP(socketServidor, "--failed");
                        try {
                            credenciais = new String[]{InetAddress.getLocalHost().getHostAddress(),
                                Servidor.getPort(),
                                Double.toString(Double.MAX_VALUE),
                                Double.toString(Double.MAX_VALUE)};

                            ServidorController.getInstance().addRotas(idSensor, credenciais);
                        } catch (UnknownHostException e) {
                        }

                    } else {
                        ServidorController.getInstance().addRotas(idSensor, credenciais);
                        ServidorController.getInstance().enviarCredenciais(socketServidor, credenciais);
                    }
                }
            } // Realiza a conexão de um servidor de borda ao servidor nuvem
            else if (acaoRequisitada.getAcao().equals("--edge-connect")) {

                if (acaoRequisitada.getObjeto() instanceof Object[]) {
                    Object[] dados = (Object[]) acaoRequisitada.getObjeto();

                    //Separa os dados
                    int x = (int) dados[0];
                    int y = (int) dados[1];
                    int porta = (int) dados[2];
                    String ip = (String) dados[3];

                    System.out.println("Criando conexão com o servidor de borda em X: " + x + " e Y: " + y);

                    //Conecta e confirma
                    ServidorController.getInstance().conectarBorda(x, y, porta, ip);
                    ServidorController.getInstance().responderTCP(socketServidor, "--connected");

                    System.out.println("Concluído");
                }
            } else if (acaoRequisitada.getAcao().equals("--recalculate")) {

                if (acaoRequisitada.getObjeto() instanceof Object[]) {
                    Object[] dados = (Object[]) acaoRequisitada.getObjeto();

                    //Separa os dados
                    int x = (int) dados[0];
                    int y = (int) dados[1];
                    String idSensor = (String) dados[2];

                    System.out.println("Buscando servidor próximo de X: " + x + " e Y: " + y);

                    String[] resposta = ServidorController.getInstance().calcularRota(x, y);

                    if (resposta.length == 0 || resposta[0] == null) { //Se nenhum servidor de borda estiver disponível a operação falha
                        ServidorController.getInstance().responderTCP(socketServidor, "--failed");
                        try {
                            resposta = new String[]{InetAddress.getLocalHost().getHostAddress(),
                                Servidor.getPort(),
                                Double.toString(Double.MAX_VALUE),
                                Double.toString(Double.MAX_VALUE)};

                            ServidorController.getInstance().addRotas(idSensor, resposta);
                        } catch (UnknownHostException e) {
                        }
                    } else {
                        String[] credenciais = {resposta[0], resposta[1]}; //ip e porta

                        ServidorController.getInstance().addRotas(idSensor, resposta);
                        ServidorController.getInstance().enviarCredenciais(socketServidor, credenciais);
                    }

                    System.out.println("Concluído");
                }
            } //Altera o servidor de borda que o sensor se conecta. Ignora um servidor especificado.
            else if (acaoRequisitada.getAcao().equals("--change-edge")) {
                if (acaoRequisitada.getObjeto() instanceof Object[]) {
                    Object[] dados = (Object[]) acaoRequisitada.getObjeto();

                    //Separa os dados
                    int x = (int) dados[0];
                    int y = (int) dados[1];
                    int porta = (int) dados[2];
                    String ip = (String) dados[3];
                    String idSensor = (String) dados[4];

                    System.out.println("Buscando servidor próximo de X: " + x + " e Y: " + y + " --ignorando " + ip + ":" + porta);

                    String[] resposta = ServidorController.getInstance().recalcularRota(x, y, ip, porta);

                    if (resposta.length == 0 || resposta[0] == null) { //Se nenhum servidor de borda estiver disponível a operação falha
                        ServidorController.getInstance().responderTCP(socketServidor, "--failed");
                        try {
                            resposta = new String[]{InetAddress.getLocalHost().getHostAddress(),
                                Servidor.getPort(),
                                Double.toString(Double.MAX_VALUE),
                                Double.toString(Double.MAX_VALUE)};

                            ServidorController.getInstance().addRotas(idSensor, resposta);
                        } catch (UnknownHostException e) {
                        }
                    } else {
                        String[] credenciais = {resposta[0], resposta[1]}; //ip e porta
                        ServidorController.getInstance().addRotas(idSensor, resposta);
                        ServidorController.getInstance().enviarCredenciais(socketServidor, credenciais);
                    }

                    System.out.println("Concluído");
                }
            } //Responde a uma solicitação de ping
            else if (acaoRequisitada.getAcao().equals("--ping-doctor")) {
                String idCliente = (String) acaoRequisitada.getObjeto();
                ServidorController.getInstance().responderPingTCP(socketServidor, "ok", idCliente);
            } //Responde a uma solicitação de ping
            else if (acaoRequisitada.getAcao().equals("--ping-edge")) {
                int[] dadosServidor = (int[]) acaoRequisitada.getObjeto();
                String ipServidor = socketServidor.getInetAddress().getHostAddress();
                ServidorController.getInstance().responderPingBorda(socketServidor, "ok", ipServidor, dadosServidor[0], dadosServidor[1], dadosServidor[2]);
            } //Salva os dados dos pacientes em risco
            
            //Salva os dados dos pacientes em risco
            else if (acaoRequisitada.getAcao().equals("--save-risk")) {

                if (acaoRequisitada.getObjeto() instanceof ArrayList) {
                    //Recebe os dados processados pela borda
                    ArrayList<Paciente> pacientes = (ArrayList<Paciente>) acaoRequisitada.getObjeto();

                    System.out.println("Salvando dados do(s) " + pacientes.size() + " pacientes em risco");

                    for (Paciente paciente : pacientes) {
                        ServidorController.getInstance().salvarPaciente(paciente, ip, porta);
                    }
                }
            } 
            //Resgata as informações do histórico armazenadas nos servidores de borda
            else if (acaoRequisitada.getAcao().equals("--rescue-cache")) {
                if (acaoRequisitada.getObjeto() instanceof ArrayList) {
                    System.out.println("Recuperando dados de cache do servidor de borda");
                    ArrayList<String[]> pacientes = (ArrayList<String[]>) acaoRequisitada.getObjeto();

                    for (int i = 0; i < pacientes.size(); i++) {
                        String[] dados = pacientes.get(i);

                        String idPaciente = dados[5];
                        Sensor sensor = new Sensor(idPaciente);

                        sensor.setBatimentos(Integer.parseInt(dados[1]));
                        sensor.setPressaoSistolica(Integer.parseInt(dados[2]));
                        sensor.setPressaoDiastolica(Integer.parseInt(dados[3]));
                        sensor.setEmRepouso(Boolean.parseBoolean(dados[4]));

                        Paciente paciente = new Paciente(idPaciente, sensor);
                        ServidorController.getInstance().salvarCacheBorda(paciente, dados[0]);
                    }
                }
            } //Realiza o login do médico
            else if (acaoRequisitada.getAcao().equals("--login")) {
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                ServidorController.getInstance().salvarMedico(credenciais[2], socketServidor.getInetAddress(), socketServidor.getPort());
                ServidorController.getInstance().logar(credenciais[0], credenciais[1], socketServidor);
            } 
            //Retorna a lista de pacientes em risco
            else if (acaoRequisitada.getAcao().equals("--list")) {

                if (acaoRequisitada.getObjeto() instanceof String) {
                    String filtro = (String) acaoRequisitada.getObjeto();

                    if (filtro.equals("-all")) {
                        ServidorController.getInstance().listarTodos(this.ip, this.porta);
                    }
                }
            } 
            //Verifica se um médico deseja monitorar um paciente
            else if (acaoRequisitada.getAcao().equals("--watch")) {

                if (acaoRequisitada.getObjeto() instanceof String) {
                    String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                    ServidorController.getInstance().atualizarPacienteEspecifico(credenciais[1], ip, porta); //(idMedico,idPaciente)
                }
            } 
            //Verifica se um médico está a procura de um sensor
            else if (acaoRequisitada.getAcao().equals("--search-sensor")) {
                if (acaoRequisitada.getObjeto() instanceof String) {
                    String idPaciente = ((String) acaoRequisitada.getObjeto());
                    ServidorController.getInstance().buscarBorda(idPaciente, socketServidor);
                }
            } else if (acaoRequisitada.getAcao().equals("--logout")) {
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                ServidorController.getInstance().logout(credenciais[0], credenciais[1], socketServidor); //[0] = tipoCliente [1] = idCliente 
            } 
            //Verifica se um médico deseja consultar o histórico de um paciente
            else if (acaoRequisitada.getAcao().equals("--history")) {
                ServidorController.getInstance().recuperarHistorico((String) acaoRequisitada.getObjeto(), socketServidor);
            }

            //Verifica se um sensor deseja realizar uma conexão
            if (acaoRequisitada.getAcao().equals("--connect-sensor-edge")) {
                ServidorController.getInstance().responderTCP(socketServidor, "accepted");
            } 
            // Verifica o ping do sensor
            else if (acaoRequisitada.getAcao().equals("--ping-sensor")) {
                Object[] credenciais = (Object[]) acaoRequisitada.getObjeto();
                String ipBorda = (String) credenciais[0];
                int portaBorda = (int) credenciais[1];
                String idCliente = (String) credenciais[2];
                int[] coordenadas = ServidorController.getInstance().getCoordenadasBorda(ipBorda, portaBorda);

                if (coordenadas != null) {
                    ServidorController.getInstance().addRotas(idCliente, new String[]{ipBorda, Integer.toString(portaBorda), Integer.toString(coordenadas[0]), Integer.toString(coordenadas[1])});
                }

                ServidorController.getInstance().responderPingSensor(socketServidor, idCliente);
            } 
            // Verifica se um sensor deseja guardar suas informações
            else if (acaoRequisitada.getAcao().equals("--save-sensor")) {
                Paciente paciente = (Paciente) acaoRequisitada.getObjeto();

                System.out.println("ID = " + paciente.getSensor().getId()
                        + " Nome = " + paciente.getNome()
                        + " Batimentos = " + paciente.getSensor().getBatimentos()
                        + " Pressão Sistólica = " + paciente.getSensor().getPressaoSistolica()
                        + " Pressão Diastólica = " + paciente.getSensor().getPressaoDiastolica());

                if (ServidorController.getInstance().validarPaciente(paciente)) {
                    ServidorController.getInstance().salvarPaciente(paciente, ip, porta);
                }
            } 
            //Verifica se o sensor solicitou a desconexão
            else if (acaoRequisitada.getAcao().equals("--logout-sensor")) {
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                ServidorController.getInstance().logout("Paciente", credenciais[1], socketServidor); //[0] = tipoCliente [1] = idCliente 
            } 


        }

    }

}
