package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;
import controller.ServidorController;

public class Servidor implements Runnable {

    private DatagramSocket socketUDPServidor;
    private ServerSocket socketTCPServidor;
    protected static int portaServidor;
    protected static String ipServidor;

    public static void main(String[] args) {
        String listen = JOptionPane.showInputDialog("Porta a ser ouvida:"); //Solicita a porta a ser ouvida
        Servidor s = new Servidor(listen);
        new Thread(s).start();
    }

    /**
     * Construtor
     *
     * @param listen - A porta a ser ouvida.
     */
    private Servidor(String listen) {
        int porta = 0;
        if (listen.isEmpty() || listen.trim().isEmpty()) { //Verifica se a porta foi informada
            System.err.println("A porta deve ser informada");
            System.exit(1);
        } else {
            porta = Integer.parseInt(listen);
            Servidor.portaServidor = porta;
            try {
                Servidor.ipServidor = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
            }
        }

        try {
            socketUDPServidor = new DatagramSocket(porta);
            socketTCPServidor = new ServerSocket(porta);
            System.out.println("Rodando em " + ipServidor + ":" + portaServidor);
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        //Inicializa as rotinas do servidor.
        ServidorController.getInstance().retomarRotas();
        ServidorController.getInstance().setIPServidor(ipServidor);
        ServidorController.getInstance().setPortaServidor(portaServidor);
        ServidorController.getInstance().monitorarServidores();
        ServidorController.getInstance().atualizarConectados();
        ouvirUDP();
        ouvirTCP();
    }

    /**
     * Ouve a porta que utiliza UDP
     */
    private void ouvirUDP() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        byte[] dadosRecebidos = new byte[1024]; //Configura o buffer de entrada
                        DatagramPacket pacote = new DatagramPacket(dadosRecebidos, dadosRecebidos.length); //Configura o pacote
                        socketUDPServidor.receive(pacote); // Aguarda o recebimento de um pacote
                        new Thread(new Gerenciador(pacote)).start(); //Inicia uma Thread para processar a requisição

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Ouve a porta que utiliza TCP
     */
    private void ouvirTCP() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        Socket recebido = socketTCPServidor.accept(); //Espera o recebimento de um dado
                        Gerenciador c = new Gerenciador(recebido);
                        new Thread(c).start(); //Inicia uma nova Thread para processar a requisição recebida
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    protected static String getPort() {
        return Integer.toString(portaServidor);
    }
}
