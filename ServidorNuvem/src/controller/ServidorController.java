package controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Conexao;
import model.ConexaoBorda;
import model.Paciente;
import model.Sensor;
import util.MensagemUDP;

public class ServidorController {

    //Declaração das TAD's
    private Map<String, Conexao> conexoes = new ConcurrentHashMap<String, Conexao>(); // Armazena as conexões com os clientes <idCliente,Conexao>
    private Map<String, ConexaoBorda> servidoresBorda = new ConcurrentHashMap<String, ConexaoBorda>(); // Armazena as conexões com os servidores de borda.
    private Map<String, Object[]> rotas = new ConcurrentHashMap<String, Object[]>(); //<idSensor,[ipServidor,portaServidor]>

    private int portaServidor;
    private String ipServidor;

    //Singleton
    private static ServidorController controller;

    private ServidorController() {
    }

    public static ServidorController getInstance() {
        if (controller == null) {
            controller = new ServidorController();
        }
        return controller;
    }

    //********************************* Rotinas de envio **********************************************\\
    /**
     * Envia uma mensagem usando UDP.
     *
     * @param mensagem MensagemUDP - A mensagem a ser enviada.
     */
    public synchronized void enviarUDP(MensagemUDP mensagem) {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte[] msg = serializarMensagens(mensagem); // Transforma a mensagem em um array de bytes

            DatagramPacket dados = new DatagramPacket(msg, msg.length, mensagem.getDestinoIP(), mensagem.getDestinoPorta()); //Prepara a conexão
            socket.send(dados);// Envia os dados
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia uma mensagem de resposta usando TCP.
     *
     * @param socket - O Socket da conexão
     * @param mensagem - A mensagem a ser enviada
     */
    public synchronized void responderTCP(Socket socket, String mensagem) {
        try {
            //Envia a mensagem
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia as credenciais do servidor de borda para o sensor usando TCP.
     *
     * @param socket - O Socket da conexão
     * @param mensagem - A mensagem a ser enviada
     */
    public synchronized void enviarCredenciais(Socket socket, String[] mensagem) {
        try {
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Envia a resposta para o cliente que enviou "Ping".
     *
     * @param socket - O socket da conexão
     * @param mensagem - A mensagem a ser enviada.
     * @param idCliente - O id do cliente destinatário.
     */
    public void responderPingTCP(Socket socketServidor, String mensagem, String idCliente) {
        try {
            ObjectOutputStream saida = new ObjectOutputStream(socketServidor.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia a resposta para o cliente que enviou "Ping".
     *
     * @param socket - O socket da conexão
     * @param idCliente - O id do cliente destinatário.
     */
    public synchronized void responderPingSensor(Socket socket, String idCliente) {

        Conexao c = conexoes.get(idCliente); // Obtém o cliente desejado

        if (c != null) { // Verifica se a conexão não é nula
            conexoes.get(idCliente).setUltimaAtualizacao(System.currentTimeMillis()); //Registra o momento da solicitação recebida pelo servidor.
        }

        try {
            String resposta = "ok";
            Object[] rota = rotas.get(idCliente);
            String ip = (String) rota[0];
            String porta = (String) rota[1];
            System.out.println("Rota:" + ip + ":" + porta);

            if (servidoresBorda.size() > 0
                    && (ip.equals(this.ipServidor) && porta.equals(Integer.toString(portaServidor))
                    || (ip.equals("127.0.0.1") && porta.equals(Integer.toString(portaServidor))))) {

                resposta = "ok--new-server";
            } else {
                resposta = "ok";
            }
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(resposta);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Envia a resposta para o servidor de borda que enviou "Ping"
     *
     * @param socket - O socket da conexão
     * @param mensagem - A mensagem a ser enviada.
     * @param idServidor - O id do servidor
     * @param x - A coordenada x do servidor de borda
     * @param y - A coordenada y do servidor de borda
     *
     */
    public void responderPingBorda(Socket socketServidor, String mensagem, String ipServidor, int portaServidor, int x, int y) {
        System.out.println("Conexão com " + ipServidor + ":" + portaServidor + " confirmada.");
        conectarBorda(x, y, portaServidor, ipServidor);
        try {
            ObjectOutputStream saida = new ObjectOutputStream(socketServidor.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //********************************* Rotinas de persistência ****************************************\\
    
    /**
     * Persiste as rotas atualizadas em um arquivo no disco
     */
    
    public synchronized void guardarRotas() {
        Path pasta = Paths.get("rotas").toAbsolutePath();
        Path arquivo = Paths.get("rotas", "rotas.txt").toAbsolutePath();

        /* Estrutura do arquivo
         * 
         * o id do sensor[]o ip[]a porta >>
         * */
        try {
            if (Files.notExists(pasta)) {
                Files.createDirectories(pasta);
            }

            if (Files.notExists(arquivo)) {
                Files.createFile(arquivo);
            } else {//Recria o arquivo
                Files.delete(arquivo);
                Files.createFile(arquivo);
            }

            Iterator<String> iterador = rotas.keySet().iterator();

            while (iterador.hasNext()) {
                String id = iterador.next();
                Object[] dados = rotas.get(id);

                List<String> lista = Arrays.asList(id + "[]" + (String) dados[0] + "[]" + dados[1] + "[]" + dados[2] + "[]" + dados[3] + ">>");
                Files.write(arquivo, lista, Charset.forName("UTF-8"), StandardOpenOption.APPEND); // Escreve no fim do arquivo
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Recupera as rotas que foram salvas em arquivos.
     */
    public synchronized void retomarRotas() {
        Path arquivo = Paths.get("rotas", "rotas.txt").toAbsolutePath();

        try {
            if (Files.notExists(arquivo)) {
                return;
            } else {//Recupera os dados do arquivo
                File file = new File(arquivo.normalize().toString());

                Scanner s = new Scanner(file);
                s.useDelimiter(">>");
                System.out.println("Recuperando rotas");
                while (s.hasNext()) {
                    String[] dados = s.next().split("\\[\\]");
                    if (dados.length >= 5) {
                        Object[] server = {dados[1], dados[2], dados[3], dados[4]};
                        System.out.println("Rota: " + (String) dados[0] + " conectado à " + (String) dados[1] + ":" + (String) dados[2]);
                        rotas.put(dados[0], server);
                        conectarBorda(Integer.parseInt(dados[3]), Integer.parseInt(dados[4]), Integer.parseInt(dados[2]), dados[1]);
                    }
                }
                s.close();
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Salva o histórico dos momentos em que o paciente esteve propenso.
     *
     * @param Paciente - O paciente propenso.
     */
    private synchronized void salvarHistorico(Paciente paciente) {
        Path pasta = Paths.get("history").toAbsolutePath(); //Obtém o caminho da pasta
        try {
            if (Files.notExists(pasta)) { // Verifica se a pasta não existe.
                Files.createDirectory(pasta); //Caso não exista, a pasta é criada
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Path pastaRaiz = Paths.get("history", paciente.getSensor().getId() + ".txt").toAbsolutePath(); //Obtém o arquivo do histórico

        SimpleDateFormat dtf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");// Cria uma máscara para a data e o horário
        Date date = new Date();

        //Guarda as informações do paciente
        List<String> dados = Arrays.asList("horario[" + dtf.format(date) + "]",
                "batimentos[" + paciente.getSensor().getBatimentos() + "]",
                "pressaoSistolica[" + paciente.getSensor().getPressaoSistolica() + "]",
                "pressaoDiastolica[" + paciente.getSensor().getPressaoDiastolica() + "]",
                "emRepouso[" + paciente.getSensor().estaEmRepouso() + "]",
                "}}");

        try {
            if (Files.notExists(pastaRaiz)) { //Cria o arquivo se ele não existir
                Files.createFile(pastaRaiz);
            }
            FileTime ultimaModificacao = Files.getLastModifiedTime(pastaRaiz); //Obtém o tempo da última modificação do arquivo
            BasicFileAttributes atributos = Files.readAttributes(pastaRaiz, BasicFileAttributes.class);
            FileTime horaCriacao = atributos.creationTime(); //Obtém o tempo de criação do arquivo
            long tempoAtual = System.currentTimeMillis();

            if (System.currentTimeMillis() - ultimaModificacao.toMillis() > 10000 || tempoAtual - horaCriacao.toMillis() < 500) { //O arquivo é atualizado a cada vinte segundos.
                Files.write(pastaRaiz, dados, Charset.forName("UTF-8"), StandardOpenOption.APPEND); // Escreve no fim do arquivo
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Recebe e guarda os dados que ficaram guardados no histórico das bordas
     * enquanto a nuvem esteve offline
     *
     * @param paciente - Os dados do paciente
     * @param horarioRegistro - O horário do registro
     */
    public synchronized void salvarCacheBorda(Paciente paciente, String horarioRegistro) {
        Path pasta = Paths.get("history").toAbsolutePath(); //Obtém o caminho da pasta
        try {
            if (Files.notExists(pasta)) { // Verifica se a pasta não existe.
                Files.createDirectory(pasta); //Caso não exista, a pasta é criada
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Path pastaRaiz = Paths.get("history", paciente.getSensor().getId() + ".txt").toAbsolutePath(); //Obtém o arquivo do histórico

        //Guarda as informações do paciente
        List<String> dados = Arrays.asList("horario[" + horarioRegistro + "]",
                "batimentos[" + paciente.getSensor().getBatimentos() + "]",
                "pressaoSistolica[" + paciente.getSensor().getPressaoSistolica() + "]",
                "pressaoDiastolica[" + paciente.getSensor().getPressaoDiastolica() + "]",
                "emRepouso[" + paciente.getSensor().estaEmRepouso() + "]",
                "}}");

        try {
            if (Files.notExists(pastaRaiz)) { //Cria o arquivo se ele não existir
                Files.createFile(pastaRaiz);
            }

            Files.write(pastaRaiz, dados, Charset.forName("UTF-8"), StandardOpenOption.APPEND); // Escreve no fim do arquivo
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //********************************* Rotinas de endereçamento ************************************** *\\
    /**
     * Este método calcula o servidor de borda mais próximo do sensor
     *
     * @param x - A coordenada x do sensor
     * @param y - A coordenada y do sensor
     *
     * @return credenciais - O endereço IP e a porta do servidor de borda
     */
    public String[] calcularRota(int x1, int y1) {

        Iterator<String> iterador = servidoresBorda.keySet().iterator();
        String[] credenciais = new String[4];
        double menorValor = Integer.MAX_VALUE;
        int xProximo = 0, yProximo = 0;

        while (iterador.hasNext()) {
            ConexaoBorda conexao = servidoresBorda.get(iterador.next());
            if (conexao.isEstavel()) {
                int x2 = conexao.getX();
                int y2 = conexao.getY();

                double parcial = Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2);
                double distancia = Math.sqrt(parcial);

                if (distancia < menorValor) {
                    menorValor = distancia;
                    credenciais[0] = conexao.getIp().toString();
                    credenciais[1] = Integer.toString(conexao.getPorta());
                    credenciais[2] = Integer.toString(conexao.getX());
                    credenciais[3] = Integer.toString(conexao.getY());
                    xProximo = x2;
                    yProximo = y2;
                }
            }

        }

        if (xProximo != 0 && yProximo != 0) {
            System.out.println("Servidor mais próximo em X: " + xProximo + " e Y:" + yProximo);
        }
        return credenciais;

    }

    /**
     * Recalcula a rota ignorando um servidor especificado
     *
     * @param x1 - A coordenada x do sensor
     * @param y1 - A coordenada y do sensor
     * @param ip - O ip do servidor a ser ignorado
     * @param porta - A porta do servidor a ser ignorada
     */
    public String[] recalcularRota(int x1, int y1, String ip, int porta) {

        Iterator<String> iterador = servidoresBorda.keySet().iterator();
        String[] credenciais = new String[4];
        double menorValor = Integer.MAX_VALUE;
        int xProximo = 0, yProximo = 0;

        while (iterador.hasNext()) {
            ConexaoBorda conexao = servidoresBorda.get(iterador.next());

            boolean mesmoIpPortaDiferente = (conexao.getIp().equals(ip) && conexao.getPorta() != porta);
            boolean mesmaPortaIpDiferente = (!conexao.getIp().equals(ip) && conexao.getPorta() == porta);

            if ((mesmoIpPortaDiferente || mesmaPortaIpDiferente) && conexao.isEstavel()) {
                System.out.println("Calculando distância até " + conexao.getIp() + ":" + conexao.getPorta());
                int x2 = conexao.getX();
                int y2 = conexao.getY();

                double parcial = Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2);
                double distancia = Math.sqrt(parcial);

                if (distancia < menorValor) {
                    menorValor = distancia;
                    credenciais[0] = conexao.getIp().toString();
                    credenciais[1] = Integer.toString(conexao.getPorta());
                    credenciais[2] = Integer.toString(conexao.getX());
                    credenciais[3] = Integer.toString(conexao.getY());
                    xProximo = x2;
                    yProximo = y2;
                }
            } else {
                conexao.setEstavel(false);
            }
        }

        if (xProximo != 0 && yProximo != 0) {
            System.out.println("Servidor mais próximo em X: " + xProximo + " e Y:" + yProximo);
        }
        return credenciais;

    }

    //********************************* Rotinas de recuperação ******************************************\\
    /**
     * Recupera o histórico de um paciente.
     *
     *
     * @param idPaciente - O ID do paciente a ser recuperado.
     * @param socket - O socket da conexão.
     */
    public synchronized void recuperarHistorico(String idPaciente, Socket socket) {
        Path pastaRaiz = Paths.get("history", idPaciente + ".txt").toAbsolutePath(); //Obtém o caminho do arquivo

        if (Files.exists(pastaRaiz)) {// Verifica se o arquivo existe
            File file = new File(pastaRaiz.normalize().toString()); //Obtém o arquivo

            try {
                ArrayList<String[]> historico = new ArrayList<String[]>(); // Lista do para armazenar as informações do histórico
                ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
                Scanner leitor = new Scanner(file);
                leitor.useDelimiter("}}"); // Separador de dados

                while (leitor.hasNext()) { // Percorre o arquivo e guarda os dados na lista do histórico
                    String[] dados = new String[5];
                    String s = leitor.next();
                    String[] cred = s.split("\\[");

                    for (int i = 1; i < cred.length; i++) {
                        dados[i - 1] = cred[i].substring(0, cred[i].indexOf("]"));
                        /*
                         * [0] - horario
                         * [1] - batimentos
                         * [2] - pressão sistólica
                         * [3] - pressão diastólica
                         * [4] - em repouso*/
                    }
                    historico.add(dados);
                }

                //Envia o arquivo
                saida.writeObject(historico);
                saida.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Busca o servidor de borda em que um paciente está conectado
     */
    public synchronized void buscarBorda(String idPaciente, Socket socket) {
        Iterator<String> iterador = conexoes.keySet().iterator();
        try {
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            while (iterador.hasNext()) { // Percorre a lista de conexões e remove a mesma se a condição a seguir for satisfeita
                String idAtual = iterador.next();

                if (idPaciente.equals(idAtual)) {
                    saida.writeObject(rotas.get(idAtual));
                    saida.flush();
                    return;
                }
            }
            saida.writeObject("--not-found");
            saida.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //********************************* Rotinas de conexões **********************************************\\
    /**
     * Cria/Atualiza uma conexão com o servidor de borda
     *
     * @param ip - O ip do servidor de borda
     * @param porta - A porta do servidor de borda
     * @param y - A coordenada y do servidor de borda
     * @param x - A coordenada x do servidor de borda
     *
     */
    public synchronized void conectarBorda(int x, int y, int porta, String ip) {
        ConexaoBorda conexao = new ConexaoBorda(ip, x, y, porta, System.currentTimeMillis()); // Constrói a conexão
        conexao.setEstavel(true);
        servidoresBorda.put(ip + ":" + porta, conexao); // Guarda a conexão
    }

    /**
     * Cria/Atualiza uma conexão com o paciente.
     *
     * @param paciente - O paciente a ser conectado.
     * @param ip - O IP do paciente.
     * @param porta - A porta do paciente.
     */
    public synchronized void salvarPaciente(Paciente paciente, InetAddress ip, int porta) {
        Conexao conexao = new Conexao(paciente.getSensor().getId(), paciente, ip, porta); // Cria uma nova conexão
        conexao.setUltimaAtualizacao(System.currentTimeMillis()); // Define o horário da última atualização
        conexoes.put(paciente.getSensor().getId(), conexao); //Adiciona a conexão ao Map
        salvarHistorico(paciente);
    }

    /**
     * Cria/Atualiza uma conexão com o médico.
     *
     * @param id - O ID do médico a ser conectado.
     * @param ip - O IP do paciente.
     * @param porta - A porta do paciente.
     */
    public synchronized void salvarMedico(String id, InetAddress ip, int porta) {
        Conexao conexao = new Conexao(id, ip, porta); // Cria uma nova conexão
        conexao.setUltimaAtualizacao(System.currentTimeMillis()); // Define o horário da última atualização
        conexoes.put(id, conexao); //Adiciona a conexão ao Map
    }

    /**
     * Guarda uma nova rota na lista do servidor
     *
     * @param idSensor - O id do sensor conectado
     * @param credenciais - Os dados do servidor de borda ao qual o sensor se
     * conectará
     *
     */
    public synchronized void addRotas(String idSensor, String[] credenciais) {
        Object[] obj = {credenciais[0], credenciais[1], credenciais[2], credenciais[3]}; //IP,Porta,X,Y
        this.rotas.put(idSensor, obj);
        guardarRotas();
    }

    //********************************* Rotinas de Login-Logout ******************************************\\
    /**
     * Realiza o login de um médico.
     *
     * @param username - O nome de usuário do médico
     * @param password - A senha do médico
     * @param socket - O socket da conexão
     */
    public synchronized void logar(String username, String password, Socket socket) {
        try {
            if (acessarConta(username, password)) { //Verifica se os dados estão corretos
                //Enviam uma resposta permitindo o acesso.
                PrintStream saida = new PrintStream(socket.getOutputStream(), true);
                saida.flush();
                saida.println("--login-accepted");
                saida.flush();

            } else {
                //Enviam uma resposta negando o acesso.
                PrintStream saida = new PrintStream(socket.getOutputStream(), true);
                saida.flush();
                saida.println("--login-refused");
                saida.flush();
            }
            socket.close();//Fecha o socket.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Busca as credenciais no arquivo de cadastros.
     *
     * @param username - O nome de usuário do médico
     * @param password - A senha do médico
     * @return true - Se os dados corresponderem
     * @return false - Se os dados não corresponderem
     */
    private synchronized boolean acessarConta(String username, String password) {
        String fs = System.getProperty("file.separator");
        Scanner bd = new Scanner(getClass().getResourceAsStream("/bd.txt"));

        while (bd.hasNext()) { //Procura no arquivo usuário e senha correspondentes aos informados
            String texto = bd.nextLine();
            String[] partes = {texto.substring(texto.indexOf("#") + 1, texto.indexOf(":")), texto.substring(texto.indexOf(":") + 1, texto.indexOf(";"))};
            if (username.equals(partes[0]) && password.equals(partes[1])) {
                bd.close();
                return true;
            }
        }
        bd.close();
        return false;
    }

    /**
     * Realiza o logout de um cliente. Envia resposta ao cliente.
     *
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     * @param socket - O socket a ser removido.
     */
    public synchronized void logout(String tipo, String idProcurado, Socket socket) {

        if (tipo.equals("Médico")) { // Verifica se o cliente é um médico
            removerConexao(tipo, idProcurado); //Remove a conexão

            // Envia a resposta para o médico
            try {
                PrintStream saida = new PrintStream(socket.getOutputStream(), true);
                saida.flush();
                saida.println("--logout-accepted");
            } catch (IOException e) {
                System.err.println("Erro de I/O");
            }

        } else if (tipo.equals("Paciente")) { // Verifica se o cliente é um paciente

            // Remove a conexão
            removerConexao(tipo, idProcurado);

            // Envia a resposta para o cliente.
            try {
                PrintStream saida = new PrintStream(socket.getOutputStream(), true);
                saida.flush();
                saida.println("--logout-accepted");
            } catch (IOException e) {
                System.err.println("Erro de I/O");
            }

        }

    }

    /**
     * Remove uma conexão existente.
     *
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     */
    private synchronized void removerConexao(String tipo, String idProcurado) {
        Iterator<String> iterador = conexoes.keySet().iterator();
        while (iterador.hasNext()) { // Percorre a lista de conexões e remove a mesma se a condição a seguir for satisfeita
            String idExistente = iterador.next();
            Conexao conexao = conexoes.get(idExistente);

            if (conexao.getTipoCliente().equals(tipo) && conexao.getId().equals(idProcurado)) { //Se a condição for satisfeita, a conexão é removida
                conexoes.remove(idExistente);
            }
        }
    }

    //********************************* Rotinas de Listagem ******************************************\\
    /**
     * Envia a lista de todos os pacientes conectados para o médico.
     *
     * @param ip - O IP do médico que receberá a lista.
     * @param porta - A porta do médico que receberá a lista.
     */
    public synchronized void listarTodos(InetAddress ip, int porta) {
        List<Paciente> pacientes = new ArrayList<Paciente>();
        Iterator<String> iterador = conexoes.keySet().iterator();

        while (iterador.hasNext()) { // Percorre a lista de conexões para selecionar os pacientes conectados.
            Conexao c = conexoes.get(iterador.next()); // Obtém uma conexão
            if (c != null) {
                if (c.getPaciente() != null) { // Se for um paciente , adiciona-o à lista.
                    pacientes.add(c.getPaciente());
                }
            }

        }

        enviarUDP(new MensagemUDP(pacientes, ip, porta, "--all"));//Envia a resposta para o cliente.
    }

    /**
     * Atualiza os dados de um paciente específico que está sendo monitorado.
     *
     * @param idPaciente - O ID do paciente monitorado.
     * @param ip - O IP do médico que receberá a resposta.
     * @param porta - A porta do médico que receberá a resposta.
     */
    public synchronized void atualizarPacienteEspecifico(String idPaciente, InetAddress ip, int porta) {

        Conexao c = conexoes.get(idPaciente);

        if (c != null) {
            Paciente paciente = conexoes.get(idPaciente).getPaciente(); // Obtém o paciente
            enviarUDP(new MensagemUDP(paciente, ip, porta, "--single")); // Envia a resposta para o médico.
        }
    }

    //********************************* Auxiliares ******************************************\\
    /**
     * Transforma uma mensagem em um array de bytes.
     *
     * @param mensagem - A mensagem a ser transformada.
     * @return byte[] - Um array de bytes convertidos
     */
    public byte[] serializarMensagens(Object mensagem) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            //Transforma a mensagem em um byte array
            ObjectOutput out = new ObjectOutputStream(b);
            out.writeObject(mensagem);
            out.flush();
            return b.toByteArray();
        } catch (IOException e) {
            System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }

    /**
     * Verifica o tempo da última atualização de um servidor de borda. Caso
     * nenhuma informação tenha sido enviada nos últimos 30 segundos, o servidor
     * de borda é retirado da lista
     *
     */
    public void monitorarServidores() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        sleep(5000);
                        Iterator<String> iterador = servidoresBorda.keySet().iterator();
                        ArrayList<String> aRemover = new ArrayList<>();
                        while (iterador.hasNext()) {
                            String chave = iterador.next();
                            ConexaoBorda c = servidoresBorda.get(chave);
                            if (System.currentTimeMillis() - c.getUltimaAtualizacao() > 15000) {
                                aRemover.add(chave);
                            }
                        }

                        for (String chave : aRemover) {
                            ConexaoBorda c = servidoresBorda.get(chave);
                            System.out.println("Desconectado-" + c.getIp() + ":" + c.getPorta());
                            servidoresBorda.remove(chave);
                        }
                    } catch (InterruptedException e) {
                    }

                }
            }
        }.start();
    }

    /**
     * Verifica periodicamente o tempo da última atualização de cada paciente da
     * lista. Caso o tempo de atualização seja maior que 7 segundos, o paciente
     * é removido.
     *
     */
    public void atualizarConectados() {
        new Thread() {
            public void run() {
                while (true) {
                    Iterator<String> i = conexoes.keySet().iterator();
                    ArrayList<String> aRemover = new ArrayList<>();

                    while (i.hasNext()) { //Percorre a lista de clientes e verifica se os mesmos fizeram alguma requisição
                        String chaveAtual = i.next();
                        long tempoAtual = System.currentTimeMillis(); //Obtém o tempo atual
                        Conexao c = conexoes.get(chaveAtual);
                        long tempoUltima = 0;
                        if (c != null) {
                            tempoUltima = c.getUltimaAtualizacao(); //Obtém a última atualização do cliente
                            if (tempoAtual - tempoUltima > 5000) { //Caso a comparação seja verdadeira, o cliente é retirado da lista de conexões.
                                aRemover.add(chaveAtual);
                            }
                        }
                    }

                    for (String chave : aRemover) {
                        conexoes.remove(chave);
                    }
                }
            }
        }.start();
    }

    /**
     * Encontra as coordenadas de um servidor de borda
     *
     * @param ipBorda - O ip do servidor de borda
     * @param portaBorda - A porta do servidor de borda
     *
     * @return int[] - O array com as coordenadas
     */
    public int[] getCoordenadasBorda(String ipBorda, int portaBorda) {
        ConexaoBorda conexao = servidoresBorda.get(ipBorda + ":" + portaBorda);
        if (conexao != null) {
            return new int[]{conexao.getX(), conexao.getY()};
        } else {
            return null;
        }
    }

    //******************************* Métodos "Críticos" *********************************************\\
    //Os métodos a seguir só serão executados caso não exista nenhum servidor de borda disponível
    /**
     * Verifica se o paciente está em risco
     *
     * @param idPaciente - O Id do paciente a ser verificado.
     */
    public synchronized boolean validarPaciente(Paciente paciente) {

        Sensor s = paciente.getSensor();
        boolean pressaoAlta = s.getPressaoSistolica() >= 14 && s.getPressaoDiastolica() >= 9;
        boolean pressaoBaixa = s.getPressaoSistolica() <= 9 && s.getPressaoDiastolica() <= 6;

        boolean bpmAltoMovimento = s.getBatimentos() > 140 && !s.estaEmRepouso();
        boolean bpmBaixoMovimento = s.getBatimentos() < 80 && !s.estaEmRepouso();

        /*No caso de alguma das condições abaixo ser satisfeita, o paciente é considerado propenso e 
         a informação é salva no histórico*/
        if ((s.estaEmRepouso() && s.getBatimentos() < 40) || (s.estaEmRepouso() && s.getBatimentos() >= 100)
                || pressaoAlta || pressaoBaixa || bpmAltoMovimento || bpmBaixoMovimento) {

            return true;
        }

        return false;
    }

    /**
     * Encontra e envia informações de um paciente específico para quem o
     * solicitou
     *
     * @param idPaciente - O id do paciente procurado
     * @param ip - O ip para o qual a resposta será enviada
     * @param porta - A porta para a qual a resposta será enviada
     *
     */
    public synchronized void monitorarEspecifico(String idPaciente, InetAddress ip, int porta) {
        Iterator<String> pacientes = conexoes.keySet().iterator();

        while (pacientes.hasNext()) {
            Conexao c = conexoes.get(pacientes.next());
            if (c.getPaciente() != null) {
                if (c.getPaciente().getSensor().getId().equals(idPaciente)) {

                    byte[] dados = serializarMensagens(new MensagemUDP(c.getPaciente(), ip, porta, "--single"));

                    try {
                        DatagramSocket socketUDPMedico = new DatagramSocket();
                        DatagramPacket pacote = new DatagramPacket(dados, dados.length, ip, porta); //Configura o pacote
                        socketUDPMedico.send(pacote); //Envia a mensagem
                    } catch (IOException e) {
                        System.err.println("Falha no envio");
                    }
                }
            }
        }

    }

    public void setPortaServidor(int porta) {
        this.portaServidor = porta;
    }

    public void setIPServidor(String ipServidor) {
        this.ipServidor = ipServidor;
    }

}
