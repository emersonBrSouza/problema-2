package controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Observable;

import exceptions.ServidorNaoRespondeuException;
import model.Paciente;
import model.Sensor;
import util.Acao;

public class SensorController extends Observable {

    private Sensor sensor;
    private String ipNuvem;
    private int portaNuvem;
    private String ipBorda;
    private int portaBorda;
    private boolean conectadoAoServidor = false;
    private Paciente paciente;
    private Thread transmitindo;
    private int valorY;
    private int valorX;
    private static SensorController controller;
    private int tentativasReconexao = 0;

    /**
     * Construtor da classe
     *
     * @param nome - O nome do cliente.
     * @param cpf - O cpf do cliente.
     * @param enderecoIPServidor - O endereço IP do servidor
     * @param porta - A porta do Servidor
     */
    public void configurarCredenciais(String nome, String cpf, String enderecoIPServidor, int portaServidor, int valorX, int valorY) {
        sensor = new Sensor(cpf);
        this.ipNuvem = enderecoIPServidor;
        this.portaNuvem = portaServidor;
        this.paciente = new Paciente(nome, sensor);
        this.valorX = valorX;
        this.valorY = valorY;
        transmitir();
    }

    private SensorController() {
    }

    public static SensorController getInstance() {
        if (controller == null) {
            controller = new SensorController();
        }

        return controller;
    }

    /**
     * O cliente requisita ao servidor nuvem o IP e a porta do servidor de borda
     * mais próximo
     */
    public synchronized boolean solicitarConexao() {

        try {

            final Socket clienteSocket = new Socket(ipNuvem, portaNuvem);

            clienteSocket.setSoTimeout(8000); //Define o tempo limite da resposta
            int[] coordenadas = {this.valorX, this.valorY};
            Object[] dados = {coordenadas, paciente.getSensor().getId()};
            final OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
            saida.write(serializarMensagens(new Acao("--connect-sensor-cloud", dados))); //Envia a solicitação de conexão ao servidor
            saida.flush();

            System.out.println("Requisitando o servidor mais próximo...");

            boolean conectou = false;

            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
            Object resposta = ((ObjectInputStream) entrada).readObject();

            if (resposta instanceof String[]) {
                String[] credenciais = (String[]) resposta;
                conectar(credenciais[0], credenciais[1]);
                conectou = true;
                System.out.println("Sensor conectado à:" + credenciais[0] + ":" + credenciais[1]);
            } else if (resposta instanceof String && ((String) resposta).equals("--failed")) {
                System.out.println("Nenhum servidor de borda encontrado.");
                conectar(ipNuvem, Integer.toString(portaNuvem));
                conectou = true;
            }

            saida.close(); //Fecha o canal
            clienteSocket.close();//Encerra o socket  

            return conectou;

        } catch (SocketTimeoutException e) {
            conectadoAoServidor = false;
            return false;
        } catch (UnknownHostException e) {
            conectadoAoServidor = false;
            System.err.println("Servidor Desconhecido");
            return false;
        } catch (IOException e) {
            conectadoAoServidor = false;
            System.err.println("Erro no envio/recebimento dos dados.");
            return false;
        } catch (ClassNotFoundException e) {
            conectadoAoServidor = false;
            System.err.println("Erro na conversão de classe.");
            return false;
        }
    }

    /**
     * O cliente conecta-se ao servidor mais próximo
     */
    public void conectar(String ipBorda, String portaBorda) {

        try {

            Socket clienteSocket = new Socket(ipBorda, Integer.parseInt(portaBorda));

            clienteSocket.setSoTimeout(8000); //Define o tempo limite da resposta
            OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
            saida.write(serializarMensagens(new Acao("--connect-sensor-edge", this.sensor.getId()))); //Envia a solicitação de conexão ao servidor
            saida.flush();

            if (ipBorda.equals(ipNuvem) && portaBorda.equals(Integer.toString(portaNuvem))) {
                System.out.println("Solicitando conexão ao servidor nuvem...");
            } else {
                System.out.println("Solicitando conexão ao servidor de borda...");
            }

            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
            Object resposta = ((ObjectInputStream) entrada).readObject();

            if (resposta instanceof String) {
                if (resposta.equals("accepted")) {
                    this.ipBorda = ipBorda;
                    this.portaBorda = Integer.parseInt(portaBorda);
                    conectadoAoServidor = true;
                    setChanged();
                    notifyObservers();

                    if (ipBorda.equals(ipNuvem) && portaBorda.equals(Integer.toString(portaNuvem))) {
                        System.out.println("Conectado ao servidor nuvem!");
                    } else {
                        System.out.println("Conectado ao servidor de borda!");
                    }

                }
            }
            saida.close(); //Fecha o canal
            clienteSocket.close();//Encerra o socket

        } catch (SocketTimeoutException e) {
            conectadoAoServidor = false;
            setChanged();
            notifyObservers();
        } catch (UnknownHostException e) {
            conectadoAoServidor = false;
            setChanged();
            notifyObservers();
            System.err.println("Servidor Desconhecido");
        } catch (IOException e) {
            conectadoAoServidor = false;
            setChanged();
            notifyObservers();
            System.err.println("Erro no envio/recebimento dos dados");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia os dados do sensor para o servidor de borda usando UDP
     */
    public void enviar() {
        byte[] dados = serializarMensagens(new Acao("--save-sensor", this.paciente)); // Instancia a mensagem

        try {
            DatagramSocket socketSensor = new DatagramSocket();
            DatagramPacket pacote = new DatagramPacket(dados, dados.length, InetAddress.getByName(ipBorda), portaBorda); //Configura o pacote
            socketSensor.send(pacote); //Envia o pacote
            socketSensor.close();
        } catch (UnknownHostException e) {
            System.err.println("Servidor não encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            System.err.println("Erro no envio dos dados");
        }
    }

    public void recalcularServidor() {
        new Thread() {
            public void run() {
                try {

                    final Socket clienteSocket = new Socket(ipNuvem, portaNuvem);

                    clienteSocket.setSoTimeout(8000); //Define o tempo limite da resposta

                    final OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída

                    if (tentativasReconexao > 3 && (!ipBorda.equals(ipNuvem) && portaNuvem != portaBorda)) { //Recalcula ignorando um servidor
                        Object[] coordenadas = {valorX, valorY, portaBorda, ipBorda, sensor.getId()};
                        saida.write(serializarMensagens(new Acao("--change-edge", coordenadas))); //Envia a solicitação de conexão ao servidor    	            	
                    } else {
                        Object[] coordenadas = {valorX, valorY, sensor.getId()};
                        saida.write(serializarMensagens(new Acao("--recalculate", coordenadas))); //Envia a solicitação de conexão ao servidor
                    }
                    saida.flush();

                    System.out.println("Requisitando o servidor mais próximo...");

                    InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
                    Object resposta = ((ObjectInputStream) entrada).readObject();

                    if (resposta instanceof String[]) {
                        String[] credenciais = (String[]) resposta;

                        if (credenciais[0].equals(ipNuvem) && credenciais[1].equals(Integer.toString(portaNuvem))) {
                            System.out.println("Conectando-se ao servidor nuvem!");
                        } else {
                            System.out.println("Sensor conectando-se à:" + credenciais[0] + ":" + credenciais[1]);
                        }

                        conectar(credenciais[0], credenciais[1]);
                        tentativasReconexao = 0;

                    } else if (resposta instanceof String && ((String) resposta).equals("--failed")) {
                        System.out.println("Não há servidores de borda disponíveis. Conectando-se diretamente à nuvem.");
                        conectar(ipNuvem, Integer.toString(portaNuvem));
                    }

                    saida.close(); //Fecha o canal
                    clienteSocket.close();//Encerra o socket  

                } catch (SocketTimeoutException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                } catch (UnknownHostException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                    System.err.println("Servidor Desconhecido");
                } catch (IOException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                    System.err.println("Erro no envio/recebimento dos dados.");
                } catch (ClassNotFoundException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                    System.err.println("Erro na conversão de classe.");
                }
            }
        }.start();
    }

    /**
     * Verifica periodicamente o estado da conexão entre o cliente e o servidor
     * de borda.
     */
    public void verificarConexao() {
        new Thread() {
            public void run() {
                try {
                    while (true) {
                        sleep(3000); // Espera três segundos para reenviar o ping
                        Socket clienteSocket = new Socket(ipBorda, portaBorda);

                        OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
                        clienteSocket.setSoTimeout(8000); //Define o tempo limite de resposta do servidor.
                        Object[] credenciais = {ipBorda, portaBorda, sensor.getId()};
                        saida.write(serializarMensagens(new Acao("--ping-sensor", credenciais))); // "Pinga o servidor"
                        saida.flush();

                        InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream());
                        Object resposta = ((ObjectInputStream) entrada).readObject();

                        if (resposta instanceof String) {
                            if (resposta.equals("ok")) { // Verifica a resposta do servidor
                                conectadoAoServidor = true;
                                setChanged();
                                notifyObservers(); //Notifica aos observadores que a conexão está estabelecida
                            } else if (resposta.equals("ok--new-server")) { //Identifica que existe um servidor de borda disponível
                                System.out.println("\nServidor de borda encontrado. Migrando...");
                                recalcularServidor(); //Recalcula a rota
                            }
                        }
                        saida.close();
                        clienteSocket.close();

                    }
                } catch (SocketTimeoutException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                } catch (SocketException e) {
                    conectadoAoServidor = false;
                    setChanged();
                    notifyObservers();
                } catch (InterruptedException | IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Transmite periodicamente os dados caso uma conexão esteja ativa.
     *
     */
    public void transmitir() {
        transmitindo = new Thread() {
            public void run() {
                verificarConexao();
                while (true) {
                    try {
                        sleep(2000); // Espera dois segundos para enviar um novo dado.
                        if (conectadoAoServidor) { // Verifica o estado da conexão
                            enviar();
                        } else {
                            if (tentativasReconexao <= 3) {
                                tentativasReconexao++;
                                setChanged();
                                notifyObservers();//Informa aos observadores que houve uma perda de conexão
                                verificarConexao();
                            } else {
                                recalcularServidor();
                            }

                        }
                    } catch (InterruptedException e) {
                        System.err.println("O sistema foi interrompido inesperadamente");
                    }
                }
            }
        };
        transmitindo.start();
    }

    /**
     * Transforma um objeto em um array de bytes
     *
     * @param mensagem - O objeto a ser transformado.
     * @return byte[] - O array de bytes.
     */
    public byte[] serializarMensagens(Object mensagem) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            //Converte o objeto em um array de bytes
            ObjectOutput out = new ObjectOutputStream(b);
            out.writeObject(mensagem);
            out.flush();
            return b.toByteArray();
        } catch (IOException e) {
            System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }

    /**
     * Atualiza localmente os dados do sensor
     *
     * @param batimentos - Os batimentos do paciente
     * @param sistolica - A pressão sistólica do paciente.
     * @param diastolica - A pressão diastólica do paciente.
     * @param emRepouso - O estado da movimentação do paciente.
     * @param x - A coordenada x do sensor
     * @param y - A coordenada y do sensor
     */
    public void atualizarDados(int batimentos, int sistolica, int diastolica, boolean emRepouso, int x, int y) {
        this.sensor.setBatimentos(batimentos);
        this.sensor.setPressaoSistolica(sistolica);
        this.sensor.setPressaoDiastolica(diastolica);
        this.sensor.setEmRepouso(emRepouso);
        this.valorX = x;
        this.valorY = y;
    }

    /**
     * Retorna o estado da conexão
     *
     * @return true - Se o paciente estiver conectado.
     * @return false - Se o paciente estiver desconectado.
     */
    public boolean estaConectado() {
        return this.conectadoAoServidor;
    }

    public int getX() {
        return this.valorX;
    }

    public int getY() {
        return this.valorY;
    }
}
