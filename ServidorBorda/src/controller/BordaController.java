package controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import model.Conexao;
import model.Paciente;
import model.Sensor;
import util.Acao;
import util.MensagemUDP;

public class BordaController {

    //Declaração das TAD's
    private Map<String, Conexao> conexoes = new ConcurrentHashMap<String, Conexao>(); // Armazena as conexões com os clientes <idCliente,Conexao>
    private Map<String, Conexao> pacientesEmRisco = new ConcurrentHashMap<String, Conexao>();
    //Endereço do Servidor Nuvem   
    private InetAddress ipNuvem;
    private int portaNuvem;
    private boolean nuvemConectada = false;

    //Posição do Servidor de Borda
    private int x;
    private int y;

    private int portaBorda;
    private Timer t;
    private Thread limpandoPersistencia;
    private Thread verificandoConexao;

    //Singleton
    private static BordaController controller;

    private BordaController() {
    }

    public static BordaController getInstance() {
        if (controller == null) {
            controller = new BordaController();
        }
        return controller;
    }

    //********************************* Rotinas de envio **********************************************\\
    /**
     * Envia uma mensagem para de resposta usando TCP.
     *
     * @param socket - O Socket da conexão
     * @param mensagem - A mensagem a ser enviada
     */
    public synchronized void responderTCP(Socket socket, String mensagem) {
        try {
            //Envia a mensagem
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia a resposta para o cliente que enviou "Ping".
     *
     * @param socket - O socket da conexão
     * @param mensagem - A mensagem a ser enviada.
     * @param idCliente - O id do cliente destinatário.
     */
    public synchronized void responderPingTCP(Socket socket, String mensagem, String idCliente) {

        Conexao c = conexoes.get(idCliente); // Obtém o cliente desejado

        if (c != null) { // Verifica se a conexão não é nula
            conexoes.get(idCliente).setUltimaAtualizacao(System.currentTimeMillis()); //Registra o momento da solicitação recebida pelo servidor.
        }

        //Envia a resposta
        try {
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(mensagem);
            saida.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Envia periodicamente a lista de pacientes em risco para a nuvem. Se a
     * lista for vazia, não envia nada
     *
     */
    public synchronized void transmitir() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        sleep(3000); //Espera 3 segundos
                        if (nuvemConectada && pacientesEmRisco != null) { //Verifica se está conectado e se a lista existe
                            if (pacientesEmRisco.size() > 0) { //Verifica se a lista tem algum paciente
                                ArrayList<Paciente> listaEnvio = new ArrayList<Paciente>();

                                for (String chave : pacientesEmRisco.keySet()) {
                                    listaEnvio.add(pacientesEmRisco.get(chave).getPaciente());
                                }

                                System.out.println(listaEnvio.size() + " pacientes em risco");
                                //Envia os dados
                                Socket socket = new Socket(ipNuvem, portaNuvem);
                                ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
                                saida.writeObject(new Acao("--save-risk", listaEnvio));
                                saida.flush();
                            }
                        }
                    } catch (IOException | InterruptedException e) {
                        System.err.println("Erro na conexão");
                    }
                }
            }
        }.start();
    }

    //********************************* Métodos de persistência **********************************************\\
    /**
     * Envia os dados dos pacientes que ficaram salvos localmente
     *
     * @param pacientes - A lista de pacientes a ser enviada para a nuvem
     *
     */
    public synchronized void salvarHistorico(ArrayList<String[]> pacientes) {
        try {

            Acao acao = new Acao("--rescue-cache", pacientes);

            Socket socket = new Socket(this.ipNuvem, this.portaNuvem);
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(acao);
            saida.flush();
        } catch (Exception e) {

        }
    }

    /**
     * Se o servidor nuvem cair, este método se responsabiliza por persistir
     * localmente o histórico do período de risco de um paciente
     *
     */
    public synchronized void persistirHistorico(Paciente paciente) {

        Path pasta = Paths.get("history-" + this.portaBorda).toAbsolutePath(); //Obtém o caminho da pasta
        try {
            if (Files.notExists(pasta)) { // Verifica se a pasta não existe.
                Files.createDirectory(pasta); //Caso não exista, a pasta é criada
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Path pastaRaiz = Paths.get("history-" + this.portaBorda, paciente.getSensor().getId() + ".txt").toAbsolutePath(); //Obtém o arquivo do histórico

        SimpleDateFormat dtf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");// Cria uma máscara para a data e o horário
        Date date = new Date();

        //Organiza as informações do paciente
        List<String> dados = Arrays.asList("horario[" + dtf.format(date) + "]",
                "batimentos[" + paciente.getSensor().getBatimentos() + "]",
                "pressaoSistolica[" + paciente.getSensor().getPressaoSistolica() + "]",
                "pressaoDiastolica[" + paciente.getSensor().getPressaoDiastolica() + "]",
                "emRepouso[" + paciente.getSensor().estaEmRepouso() + "]",
                "}}");

        try {
            if (Files.notExists(pastaRaiz)) { //Cria o arquivo se ele não existir
                Files.createFile(pastaRaiz);
            }
            FileTime ultimaModificacao = Files.getLastModifiedTime(pastaRaiz); //Obtém o tempo da última modificação do arquivo
            BasicFileAttributes atributos = Files.readAttributes(pastaRaiz, BasicFileAttributes.class);
            FileTime horaCriacao = atributos.creationTime(); //Obtém o tempo de criação do arquivo
            long tempoAtual = System.currentTimeMillis();

            if (tempoAtual - ultimaModificacao.toMillis() > 10000 || tempoAtual - horaCriacao.toMillis() < 500) { //O arquivo é atualizado a cada vinte segundos.
                System.out.println("Persistindo dados localmente. Esperando conexão...");
                Files.write(pastaRaiz, dados, Charset.forName("UTF-8"), StandardOpenOption.APPEND); // Escreve no fim do arquivo
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Verifica se a conexão com a nuvem está estabelecida e envia os dados que
     * foram persistidos enquanto a nuvem não estava disponível
     *
     */
    public void esvaziarPersistencia() {
        limpandoPersistencia = new Thread() {
            public void run() {
            	//Quando a conexão é reestabelecida, o bloco abaixo é executado.

                Path pasta = Paths.get("history-" + portaBorda).toAbsolutePath(); //Obtém o caminho da pasta
                if (Files.notExists(pasta)) { // Verifica se a pasta não existe.
                    return;
                }

                File dir = new File(pasta.toUri());

                if (dir.exists() && dir.isDirectory()) {
                    String[] arquivos = dir.list();

                    //Verifica se a pasta tem conteúdo e cada arquivo é enviado para a nuvem
                    for (String nomeArquivo : arquivos) {

                        File file = new File(Paths.get("history-" + portaBorda, nomeArquivo).normalize().toString());

                        try {
                            Scanner leitor = new Scanner(file);
                            leitor.useDelimiter("}}"); // Separador de dados
                            ArrayList<String[]> pacientes = new ArrayList<String[]>();
                            while (leitor.hasNext()) { // Percorre o arquivo
                                String[] dados = new String[6];
                                String s = leitor.next();
                                String[] cred = s.split("\\[");

                                for (int i = 1; i < cred.length - 1; i++) {
                                    dados[i - 1] = cred[i].substring(0, cred[i].indexOf("]"));
                                    /*
                                     * [0] - horario
                                     * [1] - batimentos
                                     * [2] - pressão sistólica
                                     * [3] - pressão diastólica
                                     * [4] - em repouso*/
                                }

                                if (dados[0] != null) {
                                    dados[5] = nomeArquivo.substring(0, nomeArquivo.lastIndexOf("."));
                                    pacientes.add(dados);
                                }

                            }
                            salvarHistorico(pacientes);
                            leitor.close();
                            Files.delete(Paths.get("history-" + portaBorda, nomeArquivo).toAbsolutePath()); //Deleta o arquivo atual
                        } catch (IOException e) {
                            System.err.println("Arquivo de histórico não encontrado");
                        }
                    }

                }

                try {
                    if (dir.listFiles().length == 0) { //Verifica se a pasta está vazia antes da exclusão
                        Files.deleteIfExists(Paths.get("history-" + portaBorda).toAbsolutePath()); //Apaga a pasta
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };
        limpandoPersistencia.start();
    }

    //********************************* Rotinas de conexão **********************************************\\
    /**
     * Configura a rotina de verificação de conexão 
     *
     */
    public void verificarConexao() {
        VerificarConexao verificarConexao = new VerificarConexao();
        Timer timer = new Timer();
        timer.schedule(verificarConexao, 0, 5000);
    }

    /**
     * Verifica se o cliente realizou alguma requisição no último minuto. Caso
     * isso não tenha ocorrido, o mesmo é retirado da lista de conexões.
     */
    public void atualizarConectados() {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    Iterator<String> i = conexoes.keySet().iterator();
                    ArrayList<String> aRemover = new ArrayList<>();

                    while (i.hasNext()) { //Percorre a lista de clientes e verifica se os mesmos fizeram alguma requisição
                        String chaveAtual = i.next();
                        long tempoAtual = System.currentTimeMillis(); //Obtém o tempo atual
                        Conexao c = conexoes.get(chaveAtual);
                        long tempoUltima = 0;
                        if (c != null) {
                            tempoUltima = c.getUltimaAtualizacao(); //Obtém a última atualização do cliente
                            if (tempoAtual - tempoUltima > 10000) { //Caso a comparação seja verdadeira, o cliente é retirado da lista de conexões.
                                aRemover.add(chaveAtual);
                            }
                        }
                    }

                    for (String chave : aRemover) {
                        System.out.println(chave + " desatualizado");
                        conexoes.remove(chave);
                    }
                }
            }
        }.start();
    }

    //********************************* Rotinas de Monitoramento **********************************************\\
    
    /**
     * Verifica se o paciente está enviando informações para o servidor de borda.
     * Caso não esteja, o mesmo é removido do servidor.
     */
    public void monitorarPacientes() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        sleep(2000);
                    } catch (Exception e) {
                    } // Espera 2 segundos

                    Iterator<String> iterador = pacientesEmRisco.keySet().iterator();
                    ArrayList<String> aRemover = new ArrayList<>();
                    while (iterador.hasNext()) {
                        String chave = iterador.next();
                        Conexao c = pacientesEmRisco.get(chave);

                        if (System.currentTimeMillis() - c.getUltimaAtualizacao() > 5000) {
                            aRemover.add(chave);
                        }
                    }

                    for (String chave : aRemover) {
                        pacientesEmRisco.remove(chave);
                    }
                }
            }
        }.start();
    }

    /**
     * Encontra e envia informações de um paciente específico para quem o
     * solicitou
     *
     * @param idPaciente - O id do paciente procurado
     * @param ip - O ip para o qual a resposta será enviada
     * @param porta - A porta para a qual a resposta será enviada
     *
     */
    public synchronized void monitorarEspecifico(String idPaciente, InetAddress ip, int porta) {
        Iterator<String> pacientes = conexoes.keySet().iterator();

        while (pacientes.hasNext()) {
            Conexao c = conexoes.get(pacientes.next());
            if (c.getPaciente() != null) {
                if (c.getPaciente().getSensor().getId().equals(idPaciente)) {

                    byte[] dados = serializarMensagens(new MensagemUDP(c.getPaciente(), ip, porta, "--single"));

                    try {
                        DatagramSocket socketUDPMedico = new DatagramSocket();
                        DatagramPacket pacote = new DatagramPacket(dados, dados.length, ip, porta); //Configura o pacote
                        socketUDPMedico.send(pacote); //Envia a mensagem
                    } catch (IOException e) {
                        System.err.println("Falha no envio");
                    }
                }
            }
        }

    }

    //********************************* Métodos de Login - Logout **********************************************\\
    /**
     * Solicita uma conexão com o servidor nuvem
     *
     * @param ipNuvem - O ip do servidor nuvem
     * @param portaNuvem - A porta do servidor nuvem
     * @param x - A coordenada X deste servidor de borda
     * @param y - A coordenada Y deste servidor de borda
     * @param portaBorda - A porta que este servidor de borda está ouvindo
     *
     * @return true - Se a conexão for bem-sucedida
     * @return false - Se a conexão for malsucedida
     *
     */
    public boolean logar(String ipNuvem, int portaNuvem, int x, int y, int portaBorda) {

        try {
            //Configura os atributos
            this.ipNuvem = InetAddress.getByName(ipNuvem);
            this.portaNuvem = portaNuvem;
            this.x = x;
            this.y = y;
            this.portaBorda = portaBorda;

            Object[] coordenadas = {this.x, this.y, this.portaBorda, ipNuvem}; //Define os dados a serem enviados

            System.out.println("Conectando-se à nuvem");
            Socket socket = new Socket(this.ipNuvem, this.portaNuvem); //Instancia o socket

            Acao acao = new Acao("--edge-connect", coordenadas); // Monta a requisição

            //Envia a requisição
            ObjectOutputStream saida = new ObjectOutputStream(socket.getOutputStream());
            saida.writeObject(acao);
            saida.flush();

            socket.setSoTimeout(8000); //Define o tempo limite

            //Aguarda a resposta
            while (true) {
                InputStream entrada = new ObjectInputStream(socket.getInputStream());
                Object resposta = ((ObjectInputStream) entrada).readObject();
                if (resposta instanceof String) { //Verifica a resposta
                    if (((String) resposta).equals("--connected")) {
                        nuvemConectada = true; //Conexão bem-sucedida
                        return true;
                    }
                }
            }
        } catch (SocketTimeoutException e) {
            System.err.println("Tempo Limite Excedido");
            return false;
        } catch (IOException e) {
            System.err.println("Erro de I/O entre borda e nuvem");
            return false;
        } catch (ClassNotFoundException e) {
            System.err.println("Não foi possível converter o objeto");
            return false;
        }
    }

    /**
     * Cria/Atualiza uma conexão com o paciente.
     *
     * @param paciente - O paciente a ser conectado.
     * @param ip - O IP do paciente.
     * @param porta - A porta do paciente.
     */
    public synchronized void salvarPaciente(Paciente paciente, InetAddress ip, int porta) {
        Conexao conexao = new Conexao(paciente.getSensor().getId(), paciente, ip, porta); // Cria uma nova conexão
        conexao.setUltimaAtualizacao(System.currentTimeMillis()); // Define o horário da última atualização
        conexoes.put(paciente.getSensor().getId(), conexao); //Adiciona a conexão ao Map
        verificaRisco(paciente, ip, porta);
    }

    /**
     * Força a desconexão de um cliente. Não envia resposta ao cliente.
     *
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     *
     */
    private synchronized void logoutForcado(String tipo, String idProcurado) {
        if (tipo.equals("Paciente")) {
            removerConexao(tipo, idProcurado);
        }
    }

    /**
     * Realiza o logout de um cliente. Envia resposta ao cliente.
     *
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     * @param socket - O socket a ser removido.
     */
    public synchronized void logout(String tipo, String idProcurado, Socket socket) {

        if (tipo.equals("Paciente")) { // Verifica se o cliente é um paciente
            // Remove a conexão
            removerConexao(tipo, idProcurado);

            // Envia a resposta para o cliente.
            try {
                PrintStream saida = new PrintStream(socket.getOutputStream(), true);
                saida.flush();
                saida.println("--logout-accepted");
            } catch (IOException e) {
                System.err.println("Erro de I/O");
            }

        }

    }

    /**
     * Remove uma conexão existente.
     *
     * @param tipo - O tipo do cliente.
     * @param idProcurado - O ID do cliente a ser removido.
     */
    private synchronized void removerConexao(String tipo, String idProcurado) {
        Iterator<String> iterador = conexoes.keySet().iterator();
        while (iterador.hasNext()) { // Percorre a lista de conexões e remove a mesma se a condição a seguir for satisfeita
            String idExistente = iterador.next();
            Conexao conexao = conexoes.get(idExistente);

            if (conexao.getTipoCliente().equals(tipo) && conexao.getId().equals(idProcurado)) { //Se a condição for satisfeita, a conexão é removida
                conexoes.remove(idExistente);
            }
        }
    }

    //*********************************  Auxiliares **********************************************\\
    /**
     * Verifica o estado do paciente.<br>
     * Faixa de valores para verificações.<br>
     * Batimentos (em movimento)-
     * <b>Alto:</b> valor > 140
     * <b>Normal:</b> valor = 100
     * <b>Baixo:</b> valor < 80 <br>
     *
     * Pressão Sist�lica (em cmHg) / Pressão Diastólica (em cmHg) -
     * <b>Alta:</b> valor >= 14 / 10
     * <b>Normal:</b> valor = 12/8 - 13/9
     * <b>Baixa:</b> valor <= 9/6 <br>
     *
     */
    private synchronized void verificaRisco(Paciente paciente, InetAddress ip, int porta) {
        Sensor s = paciente.getSensor();
        boolean pressaoAlta = s.getPressaoSistolica() >= 14 && s.getPressaoDiastolica() >= 9;
        boolean pressaoBaixa = s.getPressaoSistolica() <= 9 && s.getPressaoDiastolica() <= 6;

        boolean bpmAltoMovimento = s.getBatimentos() > 140 && !s.estaEmRepouso();
        boolean bpmBaixoMovimento = s.getBatimentos() < 80 && !s.estaEmRepouso();

        boolean estaEmPerigo = false;

        /*No caso de alguma das condições abaixo ser satisfeita, o paciente é considerado propenso e 
         a informação é salva no histórico*/
        if ((s.estaEmRepouso() && s.getBatimentos() < 40) || (s.estaEmRepouso() && s.getBatimentos() >= 100)
                || pressaoAlta || pressaoBaixa || bpmAltoMovimento || bpmBaixoMovimento) {

            estaEmPerigo = true;
        }

        System.out.println("Nuvem conectada:" + nuvemConectada);
        // A decisão depende do estado da conexão com a nuvem
        if (estaEmPerigo && nuvemConectada) {
            Conexao conexao = new Conexao(paciente.getNome(), paciente, ip, porta);
            conexao.setUltimaAtualizacao(System.currentTimeMillis());
            pacientesEmRisco.put(paciente.getSensor().getId(), conexao);
            esvaziarPersistencia();
        } else if (estaEmPerigo && !nuvemConectada) {
            persistirHistorico(paciente); // Persiste localmente até a conexão ser estabelecida novamente.                                	
        }
    }

    /**
     * Transforma uma mensagem em um array de bytes.
     *
     * @param mensagem - A mensagem a ser transformada.
     * @return byte[] - Um array de bytes convertidos
     */
    public byte[] serializarMensagens(Object mensagem) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            //Transforma a mensagem em um byte array
            ObjectOutput out = new ObjectOutputStream(b);
            out.writeObject(mensagem);
            out.flush();
            return b.toByteArray();
        } catch (IOException e) {
            System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }

    /**
     * Esta classe define a rotina para a verificação do estado da conexão entre
     * servidor de borda e servidor nuvem
     */
    private class VerificarConexao extends TimerTask {

        public void run() {
            try {
                Socket clienteSocket = new Socket(ipNuvem, portaNuvem);

                OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
                clienteSocket.setSoTimeout(8000); //Define o tempo limite de resposta do servidor.

                System.out.println("Disparando ping " + ipNuvem + ":" + portaNuvem);
                long tempoInicio = System.currentTimeMillis();

                saida.write(serializarMensagens(new Acao("--ping-edge", new int[]{portaBorda, x, y}))); // "Pinga o servidor nuvem"
                saida.flush();

                InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream());
                Object resposta = ((ObjectInputStream) entrada).readObject();

                System.out.println("Tempo de resposta = " + (System.currentTimeMillis() - tempoInicio) + "ms");
                if (resposta instanceof String) {
                    if (resposta.equals("ok")) { // Verifica a resposta do servidor
                        nuvemConectada = true;
                    }
                }
                saida.close();
                clienteSocket.close();
            } catch (SocketTimeoutException e) {
                nuvemConectada = false;
                return;
            } catch (SocketException e) {
                nuvemConectada = false;
                return;
            } catch (IOException | ClassNotFoundException e) {
                nuvemConectada = false;
                return;
            } catch (Exception e) {
                nuvemConectada = false;
                return;
            }
        }
    }
}
