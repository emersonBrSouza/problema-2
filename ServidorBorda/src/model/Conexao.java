package model;

import java.net.InetAddress;

/**
 * Esta classe guarda os dados relativos a um cliente conectado
 */
public class Conexao {

    private String id;
    private String nomeCliente;
    private InetAddress enderecoIP;
    private int porta;
    private Paciente paciente;
    private String tipoCliente;
    private long ultimaAtualizacao;

    /**
     * Construtor do cliente Paciente
     *
     * @param nomeCliente - O nome do cliente conectado
     * @param paciente - Os dados do paciente conectado
     * @param endereco - O IP do cliente conectado
     * @param porta - A porta do cliente conectado
     */
    public Conexao(String nomeCliente, Paciente paciente, InetAddress endereco, int porta) {
        this.id = paciente.getSensor().getId();
        this.nomeCliente = nomeCliente;
        this.paciente = paciente;
        this.enderecoIP = endereco;
        this.porta = porta;
        this.tipoCliente = "Paciente";
    }

    /**
     * Construtor do cliente Médico
     *
     * @param id - O CRM do médico conectado
     * @param endereco - O IP do cliente conectado
     * @param porta - A porta do cliente conectado
     */
    public Conexao(String id, InetAddress endereco, int porta) {
        this.id = id;
        this.enderecoIP = endereco;
        this.porta = porta;
        this.tipoCliente = "Médico";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public Paciente getPaciente() {
        return this.paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Sensor getSensor() {
        return this.paciente.getSensor();
    }

    public InetAddress getEnderecoIP() {
        return enderecoIP;
    }

    public void setEnderecoIP(InetAddress enderecoIP) {
        this.enderecoIP = enderecoIP;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public long getUltimaAtualizacao() {
        return ultimaAtualizacao;
    }

    public void setUltimaAtualizacao(long ultimaAtualizacao) {
        this.ultimaAtualizacao = ultimaAtualizacao;
    }

}
