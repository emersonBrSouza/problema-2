package network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;

import controller.BordaController;
import model.Paciente;
import util.Acao;

public class Gerenciador implements Runnable {

    private DatagramPacket pacote;
    private InetAddress ip;
    private int porta;
    private Object mensagem;
    private Socket socketServidor;

    /**
     * Construtor para requisições via UDP
     *
     * @param pacote - o pacote recebido
     */
    public Gerenciador(DatagramPacket pacote) {
        this.pacote = pacote;
        this.ip = pacote.getAddress();
        this.porta = pacote.getPort();
        mensagem = desserializarMensagem(pacote.getData());

    }

    /**
     * Construtor para requisições via TCP
     *
     * @param socket - O socket da conexão
     */
    public Gerenciador(Socket socket) {
        this.socketServidor = socket;
        try {
            InputStream tcpInput = new ObjectInputStream(socketServidor.getInputStream());

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream obj = new ObjectOutputStream(bytes);

            try {
                obj.writeObject(((ObjectInputStream) tcpInput).readObject());
                bytes.toByteArray();
                mensagem = desserializarMensagem(bytes.toByteArray());
            } catch (ClassNotFoundException e) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Transforma um array de bytes em um Object.
     *
     * @param data - O array de bytes recebido.
     * @return Object - O objeto desserializado.
     */
    private Object desserializarMensagem(byte[] data) {
        ByteArrayInputStream mensagem = new ByteArrayInputStream(data);

        try {
            ObjectInput leitor = new ObjectInputStream(mensagem);
            return (Object) leitor.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void run() {

        /*Define as decisões a serem tomadas a partir das requisições recebidas*/
        if (mensagem instanceof Acao) {
            Acao acaoRequisitada = (Acao) mensagem; // A ação recebida
            
            //Verifica se a mensagem indica que um sensor quer se conectar a uma borda.
            if (acaoRequisitada.getAcao().equals("--connect-sensor-edge")) {
                BordaController.getInstance().responderTCP(socketServidor, "accepted");
            } 
            //Verifica se o sensor enviou um ping para o servidor.
            else if (acaoRequisitada.getAcao().equals("--ping-sensor")) {
                Object[] credenciais = (Object[]) acaoRequisitada.getObjeto();
                String idCliente = (String) credenciais[2];
                BordaController.getInstance().responderPingTCP(socketServidor, "ok", idCliente);
            } 
            //Verifica se o sensor deseja guardar uma informação
            else if (acaoRequisitada.getAcao().equals("--save-sensor")) {
                Paciente paciente = (Paciente) acaoRequisitada.getObjeto();
                BordaController.getInstance().salvarPaciente(paciente, ip, porta);
                System.out.println("ID = " + paciente.getSensor().getId()
                        + " Nome = " + paciente.getNome()
                        + " Batimentos = " + paciente.getSensor().getBatimentos()
                        + " Pressão Sistólica = " + paciente.getSensor().getPressaoSistolica()
                        + " Pressão Diastólica = " + paciente.getSensor().getPressaoDiastolica()
                        + " Em repouso = " + paciente.getSensor().estaEmRepouso());
            } 
            //Verifica se o sensor solicitou a desconexão
            else if (acaoRequisitada.getAcao().equals("--logout-sensor")) {
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                BordaController.getInstance().logout("Paciente", credenciais[1], socketServidor); //[0] = tipoCliente [1] = idCliente 
            } 
            //Verifica se um médico deseja monitorar um paciente específico
            else if (acaoRequisitada.getAcao().equals("--watch")) {
                String[] credenciais = ((String) acaoRequisitada.getObjeto()).split(":");
                BordaController.getInstance().monitorarEspecifico(credenciais[1], ip, porta);
            } 
            //Verifica se a nuvem solicitou uma reconexão
            else if (acaoRequisitada.getAcao().equals("--verify")) {
                BordaController.getInstance().responderTCP(socketServidor, "--connected");
                BordaController.getInstance().verificarConexao();
            }
        }

    }

}
