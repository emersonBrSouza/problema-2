package network;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import controller.BordaController;

public class Servidor implements Runnable {

    private DatagramSocket socketUDPServidor;
    private ServerSocket socketTCPServidor;

    private String ip_Nuvem;
    private int porta_Nuvem;
    private int porta_Borda;
    private int posicaoX;
    private int posicaoY;
    private static Servidor servidorLocal;

    public static void main(String[] args) {
        servidorLocal = new Servidor();
    }

    /**
     * Construtor
     *
     */
    private Servidor() {
        InfoServidor dialog = new InfoServidor();
    }

    private void iniciarServidor() {
        try {
            socketUDPServidor = new DatagramSocket(porta_Borda);
            socketTCPServidor = new ServerSocket(porta_Borda);
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            System.out.println("Erro de I/O");
        }
        new Thread(servidorLocal).start();
    }

    @Override
    public void run() {

        //Verifica se existe um servidor nuvem com as credenciais informadas
        if (BordaController.getInstance().logar(this.ip_Nuvem, this.porta_Nuvem, this.posicaoX, this.posicaoY, this.porta_Borda)) {
            System.out.println("Conectado à nuvem");
        } else {
            System.err.println("Falha ao conectar com o servidor. Tente novamente mais tarde");
            System.exit(0); //Se não houver um servidor nuvem, encerra a aplicação
        }
        //Inicializa as rotinas do servidor.

        BordaController.getInstance().monitorarPacientes();
        BordaController.getInstance().atualizarConectados();
        BordaController.getInstance().verificarConexao();
        BordaController.getInstance().transmitir();
        ouvirUDP();
        ouvirTCP();
    }

    /**
     * Ouve a porta que utiliza UDP
     */
    private void ouvirUDP() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        byte[] dadosRecebidos = new byte[1024]; //Configura o buffer de entrada
                        DatagramPacket pacote = new DatagramPacket(dadosRecebidos, dadosRecebidos.length); //Configura o pacote
                        socketUDPServidor.receive(pacote); // Aguarda o recebimento de um pacote
                        new Thread(new Gerenciador(pacote)).start(); //Inicia uma Thread para processar a requisição

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Ouve a porta que utiliza TCP
     */
    private void ouvirTCP() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        Socket recebido = socketTCPServidor.accept(); //Espera o recebimento de um dado
                        Gerenciador c = new Gerenciador(recebido);
                        new Thread(c).start(); //Inicia uma nova Thread para processar a requisição recebida
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Esta classe configura o dialógo que aparece ao iniciar o servidor de
     * borda. O usuário deve informar o IP e Porta do servidor nuvem e as
     * coordenadas e porta do servidor de borda que está sendo configurado
     */
    private class InfoServidor extends JDialog {

        public InfoServidor() {
            setResizable(false);
            setBounds(100, 100, 249, 327);
            getContentPane().setLayout(null);

            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                System.err.println("Erro no look and feel");
            }

            JTextPane textoInfo = new JTextPane();
            textoInfo.setEditable(false);
            textoInfo.setBackground(getContentPane().getBackground());
            textoInfo.setText("Configuração do Servidor Nuvem");
            textoInfo.setBounds(29, 11, 214, 20);
            getContentPane().add(textoInfo);

            final JTextField ipNuvem = new JTextField();
            ipNuvem.setBounds(29, 68, 119, 20);
            getContentPane().add(ipNuvem);
            ipNuvem.setColumns(10);

            JTextPane txtpnIpDoServidor = new JTextPane();
            txtpnIpDoServidor.setEditable(false);
            txtpnIpDoServidor.setBackground(getContentPane().getBackground());
            txtpnIpDoServidor.setText("IP do Servidor Nuvem");
            txtpnIpDoServidor.setBounds(29, 42, 139, 20);
            getContentPane().add(txtpnIpDoServidor);

            JTextPane txtpnPortaDoServidor = new JTextPane();
            txtpnPortaDoServidor.setEditable(false);
            txtpnPortaDoServidor.setBackground(getContentPane().getBackground());
            txtpnPortaDoServidor.setText("Porta");
            txtpnPortaDoServidor.setBounds(165, 42, 58, 20);
            getContentPane().add(txtpnPortaDoServidor);

            final JTextField portaNuvem = new JTextField();
            portaNuvem.addKeyListener(new VerificarNumero());
            portaNuvem.setBounds(165, 68, 58, 20);
            getContentPane().add(portaNuvem);
            portaNuvem.setColumns(10);

            JTextPane txtpnCoordenadas = new JTextPane();
            txtpnCoordenadas.setEditable(false);
            txtpnCoordenadas.setBackground(getContentPane().getBackground());
            txtpnCoordenadas.setText("Coordenadas deste servidor");
            txtpnCoordenadas.setBounds(29, 99, 198, 20);
            getContentPane().add(txtpnCoordenadas);

            final JTextField campoX = new JTextField();
            campoX.addKeyListener(new VerificarNumero());
            campoX.setBounds(52, 130, 42, 20);
            getContentPane().add(campoX);
            campoX.setColumns(10);

            JTextPane txtpnX = new JTextPane();
            txtpnX.setBackground(getContentPane().getBackground());
            txtpnX.setEditable(false);
            txtpnX.setText("X:");
            txtpnX.setBounds(29, 130, 21, 20);
            getContentPane().add(txtpnX);

            JTextPane txtpnY = new JTextPane();
            txtpnY.setEditable(false);
            txtpnY.setBackground(getContentPane().getBackground());
            txtpnY.setText("Y:");
            txtpnY.setBounds(142, 130, 21, 20);
            getContentPane().add(txtpnY);

            final JTextField campoY = new JTextField();
            campoY.setBounds(165, 130, 42, 20);
            campoY.addKeyListener(new VerificarNumero());
            getContentPane().add(campoY);
            campoY.setColumns(10);

            final JTextField portaBorda = new JTextField();
            portaBorda.addKeyListener(new VerificarNumero());
            portaBorda.setBounds(29, 192, 58, 20);
            getContentPane().add(portaBorda);
            portaBorda.setColumns(10);

            JTextPane txtpnPortaDesteServidor = new JTextPane();
            txtpnPortaDesteServidor.setBackground(getContentPane().getBackground());
            txtpnPortaDesteServidor.setEditable(false);
            txtpnPortaDesteServidor.setText("Porta deste servidor");
            txtpnPortaDesteServidor.setBounds(29, 161, 159, 20);
            getContentPane().add(txtpnPortaDesteServidor);
            setLayout(null);

            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.err.println("Não foi possível iniciar o servidor. Execute novamente e preencha os dados.");
                    System.exit(0);
                }
            });

            JButton btnSalvar = new JButton("Salvar");
            btnSalvar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    try {
                        InetAddress.getByName(ipNuvem.getText());
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Insira um endereço ip válido");
                        return;
                    }

                    if (!campoVazio(ipNuvem.getText()) && !campoVazio(portaNuvem.getText()) && !campoVazio(campoX.getText())
                            && !campoVazio(campoY.getText()) && !campoVazio(portaBorda.getText())) {

                        ip_Nuvem = ipNuvem.getText();
                        porta_Nuvem = Integer.parseInt(portaNuvem.getText());
                        porta_Borda = Integer.parseInt(portaBorda.getText());
                        posicaoX = Integer.parseInt(campoX.getText());
                        posicaoY = Integer.parseInt(campoY.getText());
                        iniciarServidor();
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Preencha todos os campos");
                    }

                }
            });
            btnSalvar.setBounds(80, 244, 89, 23);
            getContentPane().add(btnSalvar);

            setVisible(true);
        }

        private boolean campoVazio(String texto) {
            if (texto.isEmpty() || texto.trim().isEmpty()) {
                return true;
            } else {
                return false;
            }
        }

        private class VerificarNumero extends KeyAdapter {

            public void keyTyped(KeyEvent e) {
                if (!Character.isDigit(e.getKeyChar())) {
                    e.consume();
                }
            }
        }
    }
}
